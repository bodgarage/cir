/*
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.
*/

CREATE TABLE tipos_pessoas (
             id INT(5) NOT NULL AUTO_INCREMENT,
			 tipoPessoa VARCHAR(50) DEFAULT NULL,

             sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
             PRIMARY KEY (id)

) ENGINE=InnoDB;

INSERT INTO `tipos_pessoas` (`id`,`tipoPessoa`,`sn_excluido`) VALUES (1,'admin','False');
INSERT INTO `tipos_pessoas` (`id`,`tipoPessoa`,`sn_excluido`) VALUES (2,'usuário avançado','False');
INSERT INTO `tipos_pessoas` (`id`,`tipoPessoa`,`sn_excluido`) VALUES (3,'usuário básico','False');


CREATE TABLE pessoas (
             id INT(5) NOT NULL AUTO_INCREMENT,
             nome VARCHAR(50) DEFAULT NULL,
             imagem_perfil BLOB(255) DEFAULT NULL,
             email VARCHAR(50) DEFAULT NULL,
             senha VARCHAR(255) DEFAULT NULL,
	     tipo_pessoa_id INT(5) DEFAULT NULL,

             sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
             PRIMARY KEY (id),
             INDEX tipo_pessoa_id (tipo_pessoa_id),
             FOREIGN KEY (tipo_pessoa_id) REFERENCES tipos_pessoas (id) ON DELETE SET NULL,
			 UNIQUE KEY email_UNIQUE (email)
) ENGINE=InnoDB;

INSERT INTO `pessoas` (`id`,`nome`,`email`,`senha`,`tipo_pessoa_id`,`sn_excluido`) VALUES (1,'CIIR Admin','coloque seu e-mail de acesso aqui','coloque sua senha com hash aqui',1,'False');

CREATE TABLE controles (
        id INT(5) NOT NULL AUTO_INCREMENT,

 	    pessoa_id INT(5) DEFAULT NULL,
	    acesso ENUM('L', 'R') NOT NULL DEFAULT 'L',
	    hardware ENUM('arduino', 'raspberry') NOT NULL DEFAULT 'raspberry',
        descricao VARCHAR(50) DEFAULT NULL,
		endereco VARCHAR(255) DEFAULT NULL,
		porta INT(5) DEFAULT NULL,

        sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
        PRIMARY KEY (id),

	    INDEX pessoa_id (pessoa_id),

	    FOREIGN KEY (pessoa_id) REFERENCES pessoas (id)

) ENGINE=InnoDB;

CREATE TABLE canais (
        id INT(5) NOT NULL AUTO_INCREMENT,

 	    pessoa_id INT(5) DEFAULT NULL,
	    controle_id INT(5) DEFAULT NULL,
	    canal INT(5) DEFAULT NULL,
        io ENUM('I', 'O') NOT NULL DEFAULT 'O',
        logicaInvertida ENUM('True', 'False') NOT NULL DEFAULT 'False',
        descricao VARCHAR(50) DEFAULT NULL,

        sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
        PRIMARY KEY (id),

	    INDEX pessoa_id (pessoa_id),
		INDEX controle_id (controle_id),

	    FOREIGN KEY (pessoa_id) REFERENCES pessoas (id),
	    FOREIGN KEY (controle_id) REFERENCES controles (id)

) ENGINE=InnoDB;


CREATE TABLE logs (
        id INT(5) NOT NULL AUTO_INCREMENT,

 	    pessoa_id INT(5) DEFAULT NULL,
 	    acao_id INT(5) DEFAULT NULL,
        ip VARCHAR(20) DEFAULT NULL,
        data DATETIME DEFAULT NULL,
		canai_id INT(5) DEFAULT NULL,
		status int(1) DEFAULT NULL,

        sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
        PRIMARY KEY (id),

	    INDEX pessoa_id (pessoa_id),
	    INDEX canai_id (canai_id),

	    FOREIGN KEY (pessoa_id) REFERENCES pessoas (id),
	    FOREIGN KEY (canai_id) REFERENCES canais (id)

) ENGINE=InnoDB;

CREATE TABLE tarefas (
        id INT(5) NOT NULL AUTO_INCREMENT,

 	    dataTarefaInicio DATETIME DEFAULT NULL,
 	    dataTarefaFim DATETIME DEFAULT NULL,
 	    horaTarefaInicio TIME DEFAULT NULL,
 	    horaTarefaFim TIME DEFAULT NULL,
 	    pessoa_id INT(5) DEFAULT NULL,
	    canai_id INT(5) DEFAULT NULL,
	    status int(1) DEFAULT NULL,
		inativo int(3) DEFAULT NULL,


        sn_excluido ENUM('True', 'False') NOT NULL DEFAULT 'False',
        PRIMARY KEY (id),

	    INDEX pessoa_id (pessoa_id),
	    INDEX canai_id (canai_id),

	    FOREIGN KEY (canai_id) REFERENCES canais (id)

) ENGINE=InnoDB;
