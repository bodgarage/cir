-- Helper function
	function file_exists(path)
	  local attr = lighty.stat(path)
	  if (attr) then
	      return true
	  else
	      return false
	  end
	end
	function removePrefix(str, prefix)
	  return str:sub(1,#prefix+1) == prefix.."/" and str:sub(#prefix+2)
	end

	--[[
	  Prefix without the trailing slash.
	  If you are _not_ serving out of a subfolder, leave `prefix` empty.
	--]]
	local prefix = ''

	-- The magic ;)
	if (not file_exists(lighty.env["physical.path"])) then
	    -- File still missing: pass it to the fastCGI backend.
	    request_uri = removePrefix(lighty.env["uri.path"], prefix)
	    if request_uri then
	      lighty.env["uri.path"]          = prefix .. "/index.php"
	      local uriquery = lighty.env["uri.query"] or ""
	      lighty.env["uri.query"] = uriquery .. (uriquery ~= "" and "&" or "") .. "url=" .. request_uri
	      lighty.env["physical.rel-path"] = lighty.env["uri.path"]
	      lighty.env["request.orig-uri"]  = lighty.env["request.uri"]
	      lighty.env["physical.path"]     = lighty.env["physical.doc-root"] .. lighty.env["physical.rel-path"]
	    end
	end
	-- Fallthrough will put it back into the lighty request loop..
	-- That means we get the 304 handling for free. ;)
