<?php
use lithium\storage\Session;
use lithium\security\Auth;

//Configure sessions
Session::config(array(
    'default' => array('adapter' => 'Cookie', 'expire' => '+7 days')
));

//Configure auth
Auth::config(array(
    'member' => array(
        'adapter' => 'Form', //Specify we're using form authentication method
        'model'   => 'Pessoas', //Specify what model is used for auth
        'fields'  => array('email', 'senha'), //Specify which fields are used
        'filters' => array('senha' => array('\lithium\util\String', 'hash'))
    ),

));
?>
