<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

use app\models\Logs;

use lithium\core\Environment;
use app\controllers\DatesController;

$this->title('Status dos controles'); ?>
<div class="panel panel-info" style="margin-top: 110px; margin-bottom: 50px">
      <div class="panel-heading">
        <h3 class="panel-title" id="panel-title">Log do sistema<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a></h3>
      </div>
      <div class="panel-body">
        <table class="table">
        <thead>
          <tr>
            <th>Data</th>
            <th>Pessoa</th>
            <th>Ação</th>
            <th>Canal</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php

          foreach ($logs as $key => $log){
          ?>
          <tr>

            <td>
            <?php

                $data = DatesController::Get($log['data']);
                echo $data = DatesController::Format($data, "d/m/Y H:i:s");
                ?>


            </td>
            <td><?=$log['pessoa']['nome'] ?></td>
            <td style="alignment-adjust: center">
                <?php

                //print_r($leitura);

                if($log['acao_id']==0){
                        ?>
                        <span class="btn btn-info btn-large">efetuou login</span>
                        <?

                }elseif($log['acao_id']==1){

                        ?>
                        <span class="btn btn-info btn-large">adicionou usuário</span>
                        <?
                }elseif($log['acao_id']==2){
                        ?>
                        <span class="btn btn-info btn-large">adicionou controle</span>
                        <?

                }elseif($log['acao_id']==3){
                        ?>
                        <span class="btn btn-info btn-large">adicionou canal</span>
                        <?

                }elseif($log['acao_id']==4){
                        ?>
                        <span class="btn btn-info btn-large">adicionou tarefa</span>
                        <?

                }elseif($log['acao_id']==5){
                   ?>
                        <span class="btn btn-info btn-large">alterou status</span>
                        <?

                }elseif($log['acao_id']==6){
                   ?>
                        <span class="btn btn-success btn-large">Monitor status</span>
                        <?

                }elseif($log['acao_id']==7){
                   ?>
                        <span class="btn btn-danger btn-large">Monitor alterou</span>
                        <?
                }elseif($log['acao_id']==8){
                   ?>
                        <span class="btn btn-warning btn-large">Servidor Down</span>
                        <?

                }
                  // print_r($log);

                ?>
            </td>
            <td><?=$log['canai']['canal']?></td>
            <td><?php


                    if($log['status']==1){
                        ?>
                        <span class="btn btn-success btn-large">Ligou</span>
                        <?
                    }elseif($log['status']==0){

                        if($log['status']!=NULL){
                        ?>

                        <span class="btn btn-default btn-large">Desligou</span>
                        <?
                        }
                    }
                        ?>
            </td>
          </tr>

          <?
          }

          ?>

        </tbody>
      </table>
      </div>
    </div>
