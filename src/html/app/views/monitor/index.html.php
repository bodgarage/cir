<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>


<?php $this->title('Status dos controles'); ?>
<div class="panel panel-info" style="margin-top: 100px;">
      <div class="panel-heading">
        <h3 class="panel-title" id="panel-title">Status dos controles<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a></h3>
      </div>
      <div class="panel-body">
          <p>Hora atual: <?=$uptime[1]?> / Estamos no ar há <?=$uptime[3]?> dia(s).</p>

        <table class="table">
        <thead>
          <tr>
            <th>Controle</th>
            <th>Dispositivo</th>
            <th>Descrição</th>
            <th>!A</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php

          foreach ($leituras as $key => $leitura){
          ?>
          <tr>
            <th scope="row"><?=$key ?></th>
            <td>
            <?php

            //print_r($leitura);

                if($leitura[1]['io']=='I'){
                    ?>
                    Dispositivo de entrada (sensor)
                    <?

                }elseif($leitura[1]['io']=='O'){
                    ?>
                    Dispositivo de saída (atuador)
                    <?

                }

            ?> - <?=$leitura[1]['controle']['endereco'] ;?>
            </td>
            <td><?=$leitura[1]['descricao'] ?></td>
            <td><?=$leitura[1]['logicaInvertida'] ?></td>

            <td style="alignment-adjust: center">
                <?php

                //print_r($leitura);

                if($leitura[0]==0){
                    if(isset($user->id)){
                    ?>

                    <a href="/monitor/atuar/<?=$leitura[1]['id'] ?>" class="btn btn-default btn-large">Desligado</a>

                    <?
                    }else{
                        ?>
                        <span class="btn btn-default btn-large">Desligado</span>
                        <?
                    }

                }elseif($leitura[0]==1){
                    if(isset($user->id)){
                    ?>

                    <a href="/monitor/atuar/<?=$leitura[1]['id'] ;?>" class="btn btn-success btn-large">Ligado</a>

                    <?
                    }else{
                        ?>
                        <span class="btn btn-success btn-large">Ligado</span>
                        <?
                    }

                }

               // print_r($leitura);
                ?>
            </td>
          </tr>

          <?
          }

          ?>

        </tbody>
      </table>
      </div>
    </div>
<?php

if(isset($user->id)){

    ?>
<script type="text/javascript">
    window.onload = Refresh;

    function Refresh() {
        setTimeout("refreshPage();", 60000);
    }
    function refreshPage() {
        window.location = location.href;
    }
</script>

<?php
}
/*
if ($MonitorEncartes1Dia){
         echo '<br>serviço de aviso para encartes fechando em 1 dia rodando';
}
else{
         echo '<br>serviço de aviso para encartes fechando em 1 dia não rodando';
}

if ($MonitorEncartes1Hora){
         echo '<br>serviço de aviso para encartes fechando em 1 hora rodando';
}
else{
         echo '<br>serviço de aviso para encartes fechando em 1 hora não rodando';
}
*/
?>
