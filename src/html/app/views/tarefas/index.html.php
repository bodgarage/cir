<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>

<div class="panel panel-info" style="margin-top: 150px; margin-bottom: 50px">
      <div class="panel-heading">
        <h3 class="panel-title" id="panel-title">Tarefas Agendadas<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a> </h3>
      </div>
      <div class="panel-body">
          <a href="/tarefas/add/" class="btn btn-success btn-large pull-right">Novo</a>
        <table class="table">
        <thead>
          <tr>
            <th>Hora Inicio</th>
            <th>Hora Final</th>
            <th>Controle</th>
            <th>Status</th>
            <th>% Inativo</th>

          </tr>
        </thead>
        <tbody>
          <?
            foreach($tarefas as $i=>$c){ ?>
                <tr class="linhalista">
                    <td>
                       <?=$c['horaTarefaInicio']?>
                    </td>
                    <td>
                       <?=$c['horaTarefaFim']?>
                    </td>
                    <td>
                       <?=$c['canai']['controle']['descricao'] ?> -
                       <?=$c['canai']['descricao']?>
                       <?php

                            if($c['canai']['controle']['hardware']=='raspberry'){
                               ?>
                               - gpio
                               <?php
                            }elseif($c['canai']['controle']['hardware']=='arduino'){
                               ?>
                               - io
                               <?php
                            }

                       ?>
                       <?=$c['canai']['canal']?>
                       <br>
                       <?php
                       if($c['canai']['controle']['acesso']=="L"){
                          echo "Local";
                       }elseif($c['canai']['controle']['acesso']=="R"){
                          echo "Remoto - ". $c['canai']['controle']['endereco'];//.":".$c['canai']['controle']['porta'];
                       }

                       ?>

                    </td>
                    <td>
                        <?php
                        if($c['status']=='0'){
                            ?>
                            Desligado
                            <?php

                        }elseif($c['status']=='1'){
                        ?>
                            Ligado
                        <?php
                        }
                        //print_r($c);
                        ?>
                    </td>
                    <td>
                        <?=$c['inativo']?>%
                    </td>
                </tr>
            <?
            }
            ?>

        </tbody>
      </table>
      </div>
    </div>

