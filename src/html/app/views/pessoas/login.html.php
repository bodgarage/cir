<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>
<div style="margin-top: 150px; margin-bottom: 50px">
<div class="modal show" id="login">
    <div class="modal-dialog">
        <div class="modal-content">
	<form id="frm_login" class="form" action="/pessoas/login/" method="post">
		<br>
	  <div class="modal-body">
	  	<?php if ($noauth): ?>
	  	<div class="alert alert-error">
		  <div>E-mail e/ou senha inválidos!</div>
		</div>
		<?php endif; ?>

	  		<label class="control-label" for="Email">E-mail</label>
				<input type="email" class="input-xlarge" id="Email" name="email">
			<label class="control-label" for="Senha">Senha</label>
				<input type="password" class="input-xlarge" id="Senha" name="senha">
	  </div>
	  <div class="modal-footer">
	    <button id="submitlogin" data-loading-text="Carregando..." rel="/pessoas/view/" class="btn btn-success btn-large">Entrar</button>
	  </div>
	 </form>
	 </div>
	 </div>
</div>
</div>

