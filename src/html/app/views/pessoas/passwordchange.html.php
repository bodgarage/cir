<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

use lithium\core\Environment;
?>
<?php $this->title('Redefinição de senha'); ?>
<?php
if($user->id){
?>
<div class="well offset3 span6" style="margin-top: 150px; margin-bottom: 50px">
<h1><?='Redefinição de senha'; ?></h1>
<p>
    <?='Para redefinir sua senha você precisa nos informar sua atual senha, a nova senha e confirmar a nova senha.'; ?>
</p>
<hr>
<p>
    <form id="frm_passwordrecovery" class="form-horizontal" action="<?=Environment::get('locale') ?>/pessoas/passwordchange/" method="post">
    <fieldset>
    <?php
    if($errors){
        ?>
        <div id="recoverynmsg" class="alert alert-error">
        <?php
        if($errors['newpassword1']){
            foreach ($errors['newpassword1'] as $key => $erro){
                echo '<p>'.$erro.'</p>';
            }

        }else{
        if($errors['pessoa']){

           foreach ($errors['pessoa'] as $key => $erro){
               echo '<p>'.$erro.'</p>';
           }
        }

        }
        ?>
        </div>
        <?php
    }
    ?>

    <div class="control-group">
            <label class="control-label" for="email"><?='Informe sua senha atual' ?></label>
            <div class="controls">
                <input type="password" class="input-xlarge" name="senha" id="senha" autocomplete="off" />
            </div>
    </div>
    <div class="control-group">
            <label class="control-label" for="email"><?='Informe sua nova senha' ?></label>
            <div class="controls">
                <input type="password" class="input-xlarge" name="newpassword1" id="newpassword1" autocomplete="off" />
            </div>
    </div>
    <div class="control-group">
            <label class="control-label" for="email"><?='Confirme sua nova senha' ?></label>
            <div class="controls">
                <input type="password" class="input-xlarge" name="newpassword2" id="newpassword2" autocomplete="off" />
            </div>
    </div>


       <div class="form-actions">
    <button id="submitlogin" data-loading-text="<?='Carregando'; ?>..." url="<?=Environment::get('locale') ?>/pessoas/passwordrecovery/" rel="<?=Environment::get('locale') ?>/pessoas/passwordrecovery/" class="btn btn-primary">
        <?='Alterar senha'; ?>
    </button>
        </div>


    </fieldset>
    </form>
</p>

</div>
<? } ?>
