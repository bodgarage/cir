<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */



foreach ($pessoas as $pessoa) {

	if(!isset($primeiro))
		$primeiro=$pessoa['id'];

	$pessoas[$pessoa['id']]['email']=$pessoa['email'];
	$pessoas[$pessoa['id']]['nome']=$pessoa['nome'];

	$pessoas[$pessoa['id']]['tipo_pessoa_id']=$pessoa['tipo_pessoa_id'];

}

?>

<div class="panel panel-info"  style="margin-top: 150px; margin-bottom: 50px">
      <div class="panel-heading">
        <h3 class="panel-title" id="panel-title">Usuários do Sistema<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a> </h3>
      </div>
      <div class="panel-body">
          <a href="/pessoas/add/" class="btn btn-success btn-large pull-right">Novo</a>
        <table class="table">
        <thead>
          <tr>
            <th>Imagem</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Tipo de conta</th>
            <th>Ações</th>

          </tr>
        </thead>
        <tbody>
          <?
            foreach($pessoas as $i=>$p){ ?>
                <tr class="linhalista">
                    <td>



                    </td>
                    <td>
                         <?=$p['nome']?>
                        <p>
                        <?
                        if($p['tipo_pessoa_id']==2||$p['tipo_pessoa_id']==3){
                            echo '<span class="acoes hide">'.$this -> html -> link('Editar', '/pessoas/edit/'.$i, array('class'=>'linkedit'));
                            //echo ' | '.$this -> html -> link('Excluir', '/pessoas/remove/'.$i).'</p>';
                            echo ' | <a data-toggle="modal" href="#confirma'.$i.'">Excluir</a></span>';
                        }?>
                        &nbsp;
                        </p>

                    </td>
                    <td>
                        <?=$p['email']?>

                    </td>
                    <td><?=$tiposPessoas[$p['tipo_pessoa_id']]['tipoPessoa']?>

                    </td>
                    <td>



                    </td>
                </tr>
            <?
            }
            ?>

        </tbody>
      </table>
      </div>
    </div>

