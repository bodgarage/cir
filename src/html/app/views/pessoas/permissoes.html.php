<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>
<table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Básico</th>
          <th>Avançado</th>
          <th>Admin</th>
        </tr>
      </thead>
      <tbody>
        <tr >
          <th scope="row">Visualizar Status</th>
          <td><a href="/monitor/index">Visualizar Status</a></td>
          <td><a href="/monitor/index">Visualizar Status</a></td>
          <td><a href="/monitor/index">Visualizar Status</a></td>
        </tr>
        <tr>
          <th scope="row">Alterar Status</th>
          <td><a href="/monitor/index">Alterar Status</a></td>
          <td><a href="/monitor/index">Alterar Status</a></td>
          <td><a href="/monitor/index">Alterar Status</a></td>
        </tr>
        <tr class="success">
          <th scope="row">Visualizar Log</th>
          <td>não disponível</td>
          <td><a href="/monitor/index">Visualizar Log</a></td>
          <td><a href="/monitor/index">Visualizar Log</a></td>
        </tr>
        <tr>
          <th scope="row">Adicionar Tarefa</th>
          <td>não disponível</td>
          <td>não disponível</td>
          <td><a href="/tarefas/add">Adicionar tarefa</a></td>
        </tr>
        <tr class="info">
          <th scope="row">Listar tarefas</th>
          <td>não disponível</td>
          <td><a href="/tarefas/index">Listar tarefa</a></td>
          <td><a href="/tarefas/index">Listar tarefa</a></td>
        </tr>
        <tr>
          <th scope="row">Visualizar usuários</th>
          <td>não disponível</td>
          <td><a href="/pessoas/index">Visualizar usuários</a></td>
          <td><a href="/pessoas/index">Visualizar usuários</a></td>
        </tr>
        <tr class="warning">
          <th scope="row">Adicionar usuários</th>
          <td>não disponível</td>
          <td>não disponível</td>
          <td><a href="/tarefas/index">Adicionar usuários</a></td>
        </tr>
        <tr>
          <th scope="row">Visualizar Controles</th>
          <td>não disponível</td>
          <td><a href="/controles/index">Visualizar Controles</a></td>
          <td><a href="/controles/index">Visualizar Controles</a></td>
        </tr>
        <tr class="danger">
          <th scope="row">Adicionar Controles</th>
          <td>não disponível</td>
          <td>não disponível</td>
          <td><a href="/controles/add">Adicionar Controles</a></td>
        </tr>
        <tr class="danger">
          <th scope="row">Reconfigurar Controles</th>
          <td>não disponível</td>
          <td>não disponível</td>
          <td><a href="/controles/reset">Reconfigurar Controles</a></td>
        </tr>

      </tbody>
    </table>
