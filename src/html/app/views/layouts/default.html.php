<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

use lithium\core\Environment;
?>
<!doctype html>
<html class=" js cssanimations csstransitions" lang="pt_BR">
	<head>
        	<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

		<?php echo $this -> html -> charset(); ?>
		<title>CIR | <?php echo $this -> title(); ?></title>
		<?php echo $this -> scripts(); ?>
		<?php echo $this -> html -> link('Icon', null, array('type' => 'icon')); ?>

		<link rel="stylesheet" href="/bootstrap/css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="/css/style.css" type="text/css">

        <link href="favicon.ico" rel="icon" type="/image/x-icon">

	    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    	<link rel="stylesheet" href="/css/style1.css">
	    <link rel="stylesheet" href="/css/prettyPhoto.css">


	</head>



	<body data-spy="scroll" data-target="#my-nav" id="cbp-so-scroller">


        <!-- Navigation Bar -->
<div class="navbar navbar-fixed-top">
	<div class="container">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="http://bodnet.ddns.net:8887"><img class="logo" src="/img/logo1.png" alt=""></a>
		</div>

		<nav id="my-nav" class="navbar-collapse collapse" role="navigation">
			<ul class="nav navbar-nav">


                <?php
                    if(isset($user->id)){
                ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Monitor<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">

                    <li><a href="/monitor/index">Status</a></li>
                    <li><a href="/logs/index">Log</a></li>

                  </ul>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Agendador<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/tarefas/add">Adicionar tarefa</a></li>
                    <li><a href="/tarefas/index">Listar tarefas</a></li>

                  </ul>
                </li>

              </ul>

              <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Administração<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/pessoas/index">Usuários</a></li>
                    <li><a href="/controles/index">Controles</a></li>
                    <li><a href="/canais/index">Canais</a></li>
                    <li><a href="/controles/reconfig">Reconfigurar Canais</a></li>

                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?=$user->nome?> <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/pessoas/edit">Editar</a></li>
                    <li><a href="/pessoas/passwordchange">Trocar senha</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="/logout">Sair</a></li>
                  </ul>
                </li>
              </ul>

            <?php
              }else{
                  ?>

        <ul class="nav navbar-nav">
            <li class="active"><a href="#home">Home <span class="sr-only">Home</span></a></li>
            <li><a href="/#about">O que é?</a></li>
            <li><a href="/#services">Detalhes</a></li>
            <li><a href="/#testimonials">Depoimentos</a></li>
            <li><a href="/#contact">Contato</a></li>
            <li><a href="/monitor/index">Monitor</a></li>
            <li><a href="/login">Login</a></li>
            <li><a href="/logs/index">Logs</a></li>


          </ul>


        <?php
              }
            ?>


		</nav><!--/.navbar-collapse -->

	</div>
</div>
<!-- End of Navigation Bar -->



		<div id="main" >

				<?php echo $this -> content(); ?>
		</div>



<!-- Footer -->
<div class="footer">
	<div class="container">

        <p>© <a href="http://bodgarage.repofy.com" style="color: #FFFFFF">Bod Garage</a> 2015. Todos direitos reservados</p>
        <p>© <a href="https://www.raspberrypi.org" style="color: #FFFFFF">Raspberry Pi</a> é uma marca registrada da Raspberry Pi Foundation</p>
	</div>
</div>

<div class="scrolltotop">
	<i class="fa fa-chevron-up"></i>
</div>
<!-- End of Footer -->




<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!--		 <script src="/js/scripts.js" type="text/javascript"></script> -->
		<script src="/bootstrap/js/bootstrap.js" type="text/javascript"></script>


<script type="text/javascript" src="/js/modernizr.custom.28468.js"></script>
<script type="text/javascript" src="/js/jquery.cslider.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/jquery.mixitup.js"></script>
<script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="/js/cbpScroller.js"></script>
<script type="text/javascript" src="/js/classie.js"></script>
<script type="text/javascript" src="/js/scripts1.js"></script>




        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-62975208-1', 'auto');
          ga('send', 'pageview');

        </script>

	</body>
</html>
