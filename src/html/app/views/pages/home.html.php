<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

use lithium\core\Environment;
use app\controllers\DatesController;

?>
<?php $this->title('Home'); ?>

<div itemscope itemtype="http://www.schema.org/WebApplication">





<section id="home" class="jumbotron" id="header">
	<div class="container" style="opacity: 1;">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="main-photo" id="header-photo" style="opacity: 1; margin-top: 0px;">
					<figure class="round-outline"><img class="round-photo" style="max-width: 200px;" src="http://bodgarage.repofy.com/wp-content/uploads/2013/12/Jumping_Goat_clip_art_hight-300x277.png"></figure>
				</div>
				<h1 style="opacity: 1; padding-top: 0px;">CIR - Controle Integrado Remoto</h1>

                <p style="opacity: 1;">Este é mais um projeto do site <a href="http://bodgarage.repofy.com" target="_blank">Bod Garage</a>.
        </p>
				<p style="opacity: 1;"><a class="btn btn-outline-white btn-big" href="#contact">Fale conosco</a></p>
			</div>
		</div>
	</div>
</section>




<section id="about" class="cbp-so-section cbp-so-init">
    <div class="container cbp-so-side cbp-so-side-top">

        <h1>O que é?</h1>

        <div class="row">
            <div class="col-sm-2 col-sm-offset-3">
                <figure class="round-outline">
                    <a href="assets/photo1.jpg" class="round-photo-anchor" rel="prettyPhoto[about-gal]">
                        <img class="round-photo img-responsive" src="assets/photo1-sm.jpg" alt="">
                        <div class="round-caption-bg"></div>
                        <i class="fa fa-search fa-lg"></i>
                    </a>
                </figure>
            </div>
            <div class="col-sm-2">
                <figure class="round-outline">
                    <a href="assets/photo2.jpg" class="round-photo-anchor" rel="prettyPhoto[about-gal]">
                        <img class="round-photo img-responsive" src="assets/photo2-sm.jpg" alt="">
                        <div class="round-caption-bg"></div>
                        <i class="fa fa-search fa-lg"></i>
                    </a>
                </figure>
            </div>
            <div class="col-sm-2">
                <figure class="round-outline">
                    <a href="assets/photo3.jpg" class="round-photo-anchor" rel="prettyPhoto[about-gal]">
                        <img class="round-photo img-responsive" src="assets/photo3-sm.jpg" alt="">
                        <div class="round-caption-bg"></div>
                        <i class="fa fa-search fa-lg"></i>
                    </a>
                </figure>
            </div>
        </div>

        <p>O Controle Integrado Remoto - CIR é uma aplicação para controle remoto de hardwares.</p>
        <p>Com ele você pode acionar a iluminação de aquários remotamente (qualquer lugar que possua internet) e agendar tarefas que devem ser desempenhadas diariamente.</p>

        <div class="row social-icons">

            <div class="col-sm-1 col-sm-offset-4" style="margin-left: 37.5%">

                <a class="icon-social icon-facebook" href="#">Facebook</a>
            </div>

            <div class="col-sm-1">
                <a class="icon-social icon-bitbucket" href="https://bitbucket.org/bodgarage/cir">BitBucket</a>
            </div>





        </div>

    </div>
</section>

<section id="services" class="cbp-so-section cbp-so-init">
    <div class="container cbp-so-side cbp-so-side-top">
        <h1>O que ele faz exatamente?</h1>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>As principais funcionalidades do funcionamento do CIR são descritas abaixo.</p>
            </div>
            <div class="col-md-3 service-column">
                <figure class="service-icon">
                    <i class="fa fa-power-off "></i>
                </figure>
                <h2><a href="#">Acionamento Remoto</a></h2>
                <p class="service-description">Ao utilizar o CIR você pode controlar remotamente diversos dispositivos, Por exemplo: luzes, motores, bombas e qualquer outro dispositivo que utilize energia elétrica para seu acionamento.</p>
            </div>
            <div class="col-md-3 service-column">
                <figure class="service-icon">
                    <i class="fa fa-calendar"></i>
                </figure>
                <h2><a href="#">Agendamento de tarefas</a></h2>
                <p class="service-description">De forma simples o CIR permite o agendamento de tarefas que necessite desempenhar diariamente. Por exemplo: manter a luz do aquário acesa durante o dia e apagar durante a noite.</p>
            </div>
            <div class="col-md-3 service-column">
                <figure class="service-icon">
                    <i class="fa fa-user-secret"></i>
                </figure>
                <h2><a href="#">Controle de usuários</a></h2>
                <p class="service-description">O CIR possui 3 niveis de usuários, são eles: administrador, usuários avançados, usuários básicos. De acordo com a hieraquia algumas funcionalidades são restritas a usuários com menos privilégios</p>
            </div>
            <div class="col-md-3 service-column">
                <figure class="service-icon">
                    <i class="fa fa-tasks "></i>
                </figure>
                <h2><a href="#">Logs</a></h2>
                <p class="service-description">As operações realizadas pelos usuários do sistema são armazenadas em log que podem ser acessados a qualquer momento.</p>
            </div>
        </div>
    </div>
</section>



<!--

<section id="testimonials" class="cbp-so-section cbp-so-init">
    <div class="container cbp-so-side cbp-so-side-top">
        <h1>O que o pessoal ta falando?</h1>
        <div class="testimonials">

            <div id="da-slider" class="da-slider">

                <div class="da-slide da-slide-current">
                    <div class="da-img">
                        <div class="round-outline">
                            <img class="round-photo" src="http://www.repofy.com/pt_BR/photos/pessoas/2/perfil/THUMB_IMG_201412100130facebook.jpg" alt="image01">
                        </div>
                    </div>
                    <p class="block-cite">Baptista Júnior, A., Desenvolvedor Bod Garage</p>
                    <blockquote><p>Entre as vantagens do CIIR, esta a vantagem de economia de energia que pode ser obtida com tarefas agendadas.</p></blockquote>
                </div>

                <div class="da-slide da-slide-current">
                    <div class="da-img">
                        <div class="round-outline">
                            <img class="round-photo" src="assets/photo-small-1.png" alt="image01">
                        </div>
                    </div>
                    <p class="block-cite">Terra, G., Desenvolvedor Bod Garage</p>
                    <blockquote><p>coloque aqui sua fala.</p></blockquote>
                </div>

                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>

            <nav class="da-dots"><span class="da-dots-current"></span><span></span><span></span><span></span></nav></div>

        </div>
    </div>
</section>  -->




<!-- Testimonials Section -->
<section id="testimonials" class="cbp-so-section cbp-so-init">
	<div class="container cbp-so-side cbp-so-side-top">
		<h1>O que o pessoal ta falando?</h1>
		<div class="testimonials">

			<div id="da-slider" class="da-slider">

				<div class="da-slide da-slide-current">
					<div class="da-img">
						<div class="round-outline">
							<img class="round-photo" src="http://www.repofy.com/pt_BR/photos/pessoas/2/perfil/THUMB_IMG_201412100130facebook.jpg" alt="image01">
						</div>
					</div>
					<p class="block-cite">Baptista Júnior, A., Desenvolvedor Bod Garage</p>
					<blockquote><p>Entre as vantagens do CIR, esta a vantagem de economia de energia que pode ser obtida com tarefas agendadas.</p></blockquote>
				</div>

				<div class="da-slide">
					<div class="da-img">
						<div class="round-outline">
							<!--<img class="round-photo" src="assets/photo-small-2.png" alt="image01">-->
						</div>
					</div>
					<p class="block-cite">Sandro Graziosi, Gerente de TI Kisa Design</p>
					<blockquote><p>O mundo está se conectando, soluções locais não são mais atraentes. CIR chegou para tornar possível, de forma prática, o monitoramento e controle de dispositivos remotos.</p></blockquote>
				</div>

				<div class="da-slide">
					<div class="da-img">
						<div class="round-outline">
							<img class="round-photo" src="assets/photo-small-3.png" alt="image01">
						</div>
					</div>
					<p class="block-cite">Jonathan Doe, founder www.mysite.com</p>
					<blockquote><p>Sed malesuada, elit quis ornare euismod, leo felis interdum velit, non ullamcorper tellus felis commodo nibh. Aliquam eget commodo risus. Proin lobortis purus vitae ornare accumsan. Proin lectus lacus, fringilla non tincidunt ac, mollis vitae magna.</p></blockquote>
				</div>

				<div class="da-slide">
					<div class="da-img">
						<div class="round-outline">
							<img class="round-photo" src="assets/photo-small-4.png" alt="image01">
						</div>
					</div>
					<p class="block-cite">Jane James, www.example.com</p>
					<blockquote><p>Praesent sed varius odio, ut ornare quam. Morbi porta sapien sed vestibulum varius. Sed vel consequat tortor. Pellentesque nec consequat libero, in ultrices libero. Donec eget libero molestie, iaculis nisl in, gravida purus.</p></blockquote>
				</div>

				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>

			<nav class="da-dots"><span class="da-dots-current"></span><span></span><span></span><span></span></nav></div>

		</div>
	</div>
</section>
<!-- End of Testimonials Section -->




<section id="contact" class="cbp-so-section cbp-so-init">
    <div class="container cbp-so-side cbp-so-side-top">
        <h1>Entre em contato</h1>
        <p>Se você gostou do projeto ente em contato.</p>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-3">
                <div class="icon">
                    <i class="fa fa-map-marker fa-2x"></i>
                </div>
                <p class="contact-meta">São Paulo, Brasil</p>
            </div>
            <div class="col-sm-2">
                <div class="icon">
                    <i class="fa fa-envelope fa-2x"></i>
                </div>
                <p class="contact-meta"></p>
            </div>
            <div class="col-sm-2">
                <div class="icon">
                    <i class="fa fa-phone fa-2x"></i>
                </div>
                <p class="contact-meta">+55 16 99727 6590</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form action="/pessoas/contato" method="post" role="form">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" id="nameinput" placeholder="Name" name="contact-name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" id="emailinput" placeholder="Email" name="contact-email">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="6" name="contact-message"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-white btn-big">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

</div>






