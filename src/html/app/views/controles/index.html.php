<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>

<div class="panel panel-info" style="margin-top: 110px;">
      <div class="panel-heading">
        <h3 class="panel-title" id="panel-title">Controles disponíveis<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a> </h3>
      </div>
      <div class="panel-body">
        <a href="/controles/add/" class="btn btn-success btn-large pull-right">Novo</a>
        <table class="table">
        <thead>
          <tr>
            <th>Acesso</th>
            <th>Hardware</th>
            <th>Descrição</th>
            <th>Endereço:Porta</th>
            <th>por</th>
            <th>Ações</th>

          </tr>
        </thead>
        <tbody>
          <?
            foreach($controles as $i=>$c){ ?>
                <tr class="linhalista">
                    <td>
                        <?php

                        if($c['acesso']=='L'){
                           echo "Local";
                        }elseif($c['acesso']=='R'){
                           echo "Remoto";
                        }


                        ?>
                    </td>
                    <td>
                        <?php
                        if($c['hardware']=='arduino'){
                            ?>
                        <img src="/img/ArduinoLogo.png" alt="Arduino é uma marca registrada." width="70">
                            <?php
                        }elseif($c['hardware']=='raspberry'){
                        ?>
                           <img src="/img/RaspberryLogo.png" alt="Raspberry Pi é uma marca registrada da Raspberry Pi Foundation." width="70">
                        <?
                        }

                        ?>
                    </td>
                    <td>
                       <?=$c['decricao'];?>

                    </td>
                    <td>
                       <?=$c['endereco']?>:<?=$c['porta']?>
                    </td>
                    <td>
                        <?=$c['pessoa']['nome']?>
                    </td>
                    <td>

                    </td>
                </tr>
            <?
            }
            ?>

        </tbody>
      </table>
      </div>
    </div>

