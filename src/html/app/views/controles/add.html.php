<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */
?>
<div style="margin-top: 150px; margin-bottom: 50px">
<h1>Novo controle</h1>
<div class="row">
	<div class="col-md-4">
<?=$this->form->create($register,array('class'=>'form')); ?>
<?php

$this->form->config(
    array(
        'templates' => array(
            'error' => '<div class="alert"><a href="#" class="close" data-dismiss="alert">×</a>{:content}</div>'
        )
    )
);

//print_r($tiposPessoas);

?>
    <label for="Tipo Pessoa">Acesso</label><?=$this->form->select('acesso', array('L'=>'Local','R'=>'Remoto')); ?>
    <br>
    <label for="Tipo Pessoa">Hardware</label><?=$this->form->select('hardware', array('raspberry'=>'Raspberry Pi','arduino'=>'Arduino')); ?>
    <?=$this->form->field(array('descricao'=>'Descrição'),array('template'=>'<div>{:label}{:input}</div>{:error}','class'=>'input')); ?>
    <?=$this->form->field(array('endereco'=>'Endereço'),array('template'=>'<div>{:label}{:input}</div>{:error}','class'=>'input')); ?>
    <?=$this->form->field(array('porta'=>'Porta'),array('template'=>'<div>{:label}{:input}</div>{:error}','class'=>'input')); ?>

    <br>
    <?=$this->form->submit('Criar novo controle',array('class'=>'btn btn-danger')); ?>
	<?=$this->form->end(); ?>
	</div>
</div>
</div>
