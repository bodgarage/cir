<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\extensions\command;

use app\controllers\MonitorController;
use app\models\Monitor;

class MonitorStatus extends \lithium\console\Command {

	public function run() {
		//$this -> header('Iniciando monitoramento de encartes');
		//$this -> out('Hello, World!');
		// using an object->method
		//$allStatus = Monitor::getStatus();
        //print_r($allStatus);
		$object = new MonitorController();
		//register_tick_function(array(&$object, 'MonitoraEncartes1Dia'), true);

		$object -> monitorStatus();

	/*	 Method 1
		declare(ticks = 1);
		while (1)
			sleep(86400);*/
	}

}
?>
