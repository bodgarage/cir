<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;

use app\models\Pessoas;
use app\models\TiposPessoas;
use app\models\Monitor;
use app\models\Controles;
use app\models\Logs;

use \lithium\security\Auth;
use \lithium\storage\Session;
use app\controllers\Images;
use lithium\core\Environment;
use lithium\util\String;

class LogsController extends \lithium\action\Controller {

    protected function _init() {
        $this -> _render['negotiate'] = true;
        parent::_init();

    }

    public function render($options = array()) {

        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }

    public function index($page=1, $limit=20) {

        //if (Auth::check('member')) {

            //if((Session::read('member.tipo_pessoa_id')==1)||(Session::read('member.tipo_pessoa_id')==2)){
                $order = array('data'=>'DESC');
                $logs = Logs::all(array('with'=>array('Pessoas','Canais'), 'order'=>$order ,'page'=>$page, 'limit' => $limit));
                $logs = $logs->to('array');
                //print_r($pessoas);exit;
                // talvez exista como fazer o lithium retornar como array



           /* }else{
                $this -> redirect('/pessoas/permissoes/');
            }

        } else {
            $this -> redirect('/pessoas/login/');
        }*/
        return compact('logs','page','limit');

    }

    public function add() {


    }

    public function edit($id=null) {

    }

    public function remove($id) {

    }

    public function view() {


    }



}
?>
