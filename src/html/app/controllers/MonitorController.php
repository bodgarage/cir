<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;


use app\models\Pessoas;
use app\models\Monitor;
use app\models\Logs;
use app\models\Tarefas;
use app\models\Controles;
use app\models\Canais;

use app\controllers\DatesController;
use \lithium\security\Auth;
use \lithium\storage\Session;
use lithium\core\Environment;
use li3_swiftmailer\mailer\Transports;

ini_set('display_errors','On');

class MonitorController extends \lithium\action\Controller {

	public function _init() {
		$this -> _render['negotiate'] = true;
		parent::_init();

	}


    public function render($options = array()) {

        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }

	public function index() {
	    $fields = array('id');
	    $controles = Controles::all(array('fields'=>$fields));
        $controles = $controles->to('array');
        //print_r($controles);
        foreach($controles as $key=>$c){
            $aux[]=$key;
        }
        $controles=$aux;

		$leituras = Monitor::getStatus($controles);
        //print_r($controles);print_r($leituras);exit;
        exec('uptime', $uptime, $retval);
        $uptime = explode(" ", $uptime[0]);
        //print_r($uptime);
        //print_r(date("Y-m-d H:i:s"));
        return compact('leituras','uptime');
	}

    public function atuar($id=null){

        if (Auth::check('member')) {
            if(is_numeric($id)){


                //print_r($canal);exit;
                $leituras = Canais::set($id);
                //print_r($leituras);exit;

                $log['pessoa_id'] = Session::read('member.id');
                $log['acao_id'] = Logs::$_CHANGESTATUS;
                $log['ip'] = $this->request->env('REMOTE_ADDR');
                $log['data']= date("Y-m-d H:i:s");
                $log['canai_id']=$id;
                //print_r($leituras['leituras']);exit;
                $newlog = Logs::create($log);
                if(!$newlog->save()){
                    echo "Falha ao gravar o Log";
                }

            }

        }else{
            return $this -> redirect('/pessoas/login/');
        }

        $this -> redirect('/monitor/');
        //$this -> set(compact('leituras'));
        //$this->render(array('template' => 'index'));


    }


	public function monitorStatus() {
		//list($sec, $usec) = explode(' ', microtime());
		//printf("[%.4f] Tick.%s\n", $sec + $usec, $str);
		//$fields = array('id');
        $controles = Controles::all(array('with'=>array('Canais')));
        $controles = $controles->to('array');
        //print_r($controles);
        foreach($controles as $key=>$c){

            //print_r($c);exit;

            $allStatus[] = Monitor::getStatus($c['id']);
        }

        foreach($allStatus as $key=>$order){
            //print_r($order);
            foreach($order as $keys=>$info){
               $aux[$keys]=$info;
            }
        }
        //print_r($aux);exit;
        $allStatus=$aux;
        //print_r($controles);
        //$allStatus = Monitor::getStatus();
        //print_r($allStatus);
        $allTarefas = Tarefas::getTarefas();
        //print_r($allTarefas);

        foreach($allTarefas['tarefas'] as $key => $tarefa){
            //print_r($tarefa);

            $horaTarefaInicio = strtotime($tarefa['horaTarefaInicio']);

            $horaTarefaFim = strtotime($tarefa['horaTarefaFim']);

            $horaatual = strtotime("now");

            //echo $horaTarefaInicio."-> inicio\n";
            //echo $horaTarefaFim."-> fim\n";
            //echo $horaatual." - > atual \n";


            if($horaTarefaInicio > $horaTarefaFim){
                   if($horaTarefaInicio > $horaatual){
                       $horaTarefaInicio = date("d/m/Y H:i:s",$horaTarefaInicio);

                       $dia = substr($horaTarefaInicio,0,2);
                       $mes = substr($horaTarefaInicio,3,2);
                       $ano = substr($horaTarefaInicio,6,4);
                       $horas = substr($horaTarefaInicio,11,2);
                       // Captura o minuto atual
                       $minutos = substr($horaTarefaInicio,14,2);
                       // Captura o segundo atual
                       $segundos = substr($horaTarefaInicio,17,2);
                       // Soma 7 horas e 45 minutos ao horário atual
                       $horaTarefaInicio = mktime($horas, $minutos, $segundos,$mes,$dia-1,$ano);
                       //print_r($horaTarefaFim);exit;

                   }else{
                       $horaTarefaFim = date("d/m/Y H:i:s",$horaTarefaFim);

                       $dia = substr($horaTarefaFim,0,2);
                       $mes = substr($horaTarefaFim,3,2);
                       $ano = substr($horaTarefaFim,6,4);
                       $horas = substr($horaTarefaFim,11,2);
                       // Captura o minuto atual
                       $minutos = substr($horaTarefaFim,14,2);
                       // Captura o segundo atual
                       $segundos = substr($horaTarefaFim,17,2);
                       // Soma 7 horas e 45 minutos ao horário atual
                       $horaTarefaFim = mktime($horas, $minutos, $segundos,$mes,$dia+1,$ano);
                       //print_r($horaTarefaFim);exit;

                   }


            }


            //echo $tarefa['status'] .' status atual canal '.$tarefa['canai']['canal'].' - '. $allStatus[$tarefa['canai']['canal']][0];

            if(($horaatual > $horaTarefaInicio)&&($horaatual < $horaTarefaFim)){

                $inativo = ($tarefa['inativo']/100)*60;
                   //print_r($inativo);
                $abaixoAtivo = 60 - $inativo;
                //echo $tarefa['inativo'].'-';
                //print_r($abaixoAtivo);

                if($tarefa['status'] != $allStatus[$tarefa['canai']['canal']][0]){
                    echo "tem que pegar ultimo registro do log ligar\n";
                    $conditions = array('acao_id'=>Logs::$_CHANGESTATUS, 'canai_id'=>$tarefa['canai']['id']);
                    $order = array('data'=>'DESC');
                    $getLog = Logs::find('first',array('conditions' => $conditions,'order'=>$order));
                    //print_r($log);
                    $horaLog = DatesController::Get($getLog->data);
                    $horaLog = DatesController::Format($horaLog, "d/m/Y H:i:s");

                    // Captura a hora atual
                    //print_r($horaLog);
                    $dia = substr($horaLog,0,2);
                    $mes = substr($horaLog,3,2);
                    $ano = substr($horaLog,6,4);
                    $horas = substr($horaLog,11,2);
                    // Captura o minuto atual
                    $minutos = substr($horaLog,14,2);
                    // Captura o segundo atual
                    $segundos = substr($horaLog,17,2);
                    // Soma 7 horas e 45 minutos ao horário atual
                    $novaHoraLog = mktime($horas, $minutos +5, $segundos,$mes,$dia,$ano);
                    // Imprime o novo horário no formato HH:MM
                    //print_r(date("d/m/Y H:i:s",$horaatual));
                    //echo "-";
                    //print_r(date("d/m/Y H:i:s", $novaHoraLog));

                    if($horaatual > $novaHoraLog){
                        //compara os minutos das horas com o tempo inativo.
                        if(substr(date("d/m/Y H:i:s",$horaatual), 14,2) < $abaixoAtivo ){
                           Canais::set($tarefa['canai']['id']);
                           $log['pessoa_id'] = 1;
                           $log['acao_id'] = Logs::$_MONITORCHANGESTATUS;
                           $log['ip'] = "localhost";
                           $log['data']= date("Y-m-d H:i:s");
                           $log['canai_id']=$tarefa['canai']['id'];
                           $newlog = Logs::create($log);
                           if(!$newlog->save()){
                               echo "Falha ao gravar o Log\n";
                           }
                           //echo "opa tem que setar";
                        }else{
                            print_r($tarefa['canai']['canal']);
                            echo "opa esta no modo inativo\n";
                        }

                    }else{
                        echo "opa não tem que setar\n";
                    }
                    //if($horaatual Log )
                    //Controles::set($tarefa['controle']['canal']);

                }else{
                    //print_r($tarefa['canai']);
                    if(($allStatus[$tarefa['canai']['canal']][0]==1)||($tarefa['canai']['logicainvertida']=='True')&&($allStatus[$tarefa['canai']['canal']][0]==0)){
                        //echo "horaatual - ".substr(date("d/m/Y H:i:s",$horaatual), 14,2)."\n";
                        //echo "abaixoativo - ". $abaixoAtivo."\n";
                        if(substr(date("d/m/Y H:i:s",$horaatual), 14,2) >= $abaixoAtivo ){
                            Canais::set($tarefa['canai']['id']);
                            $log['pessoa_id'] = 1;
                            $log['acao_id'] = Logs::$_MONITORCHANGESTATUS;
                            $log['ip'] = "localhost";
                            $log['data']= date("Y-m-d H:i:s");
                            $log['canai_id']=$tarefa['canai']['id'];
                            $newlog = Logs::create($log);
                            if(!$newlog->save()){
                               echo "Falha ao gravar o Log\n";
                            }
                            //echo "opa tem que setar";
                            print("ativou modo inativo\n");
                        }else{
                            print("não realizou ação\n");
                        }
                    }

                }

            }else{
                print("Não há nada a fazer\n");
            }

        }

        $log['pessoa_id'] = 1;
        $log['acao_id'] = Logs::$_MONITOR;
        $log['ip'] = "localhost";
        $log['data']= date("Y-m-d H:i:s");
        $log['canal']=NULL;
        $newlog = Logs::create($log);
        if(!$newlog->save()){
            echo "Falha ao gravar o Log";
        }


	}



}
?>
