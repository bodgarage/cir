<?
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;

class DatesController extends \lithium\action\Controller {

    public function add_date($givendate, $day = 0, $mth = 0, $yr = 0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d h:i:s', mktime(date('h', $cd), date('i', $cd), date('s', $cd), date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr));
        return $newdate;
    }

    /* converte d/m/Y para Y-m-d e vice-versa */
    public function converte($data) {
        return implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) == 0 ? "-" : "/", $data)));
    }

    /* pega o dia do campo datetime */
    public function dia($data) {
        return date('d/m/Y', strtotime($data));
    }

    /* pega a hora do campo datetime */
    public function hora($data) {
        return date('H:i', strtotime($data));
    }

    public function diaDaSemana($intdia){
        switch($intdia){
            case 7:
                return 'Domingo';
                break;
            case 1:
                return 'Segunda';
                break;
            case 2:
                return 'Terça';
                break;
            case 3:
                return 'Quarta';
                break;
            case 4:
                return 'Quinta';
                break;
            case 5:
                return 'Sexta';
                break;
            case 6:
                return 'Sábado';
                break;
        }
    }

    public function menorqueagora($data) {
        if (strtotime($data) <= strtotime(date('Y-m-d H:i:s'))) {
            return 1;
        }
        return 0;
    }

    public function faltaumdia($data) {
        if (DatesController::dia($data) == date('d/m/Y', strtotime("+1 day"))) {
            return 1;
        }
        return 0;
    }

    public function mysqldate($dia, $hora, $minuto) {
        $arrdata = explode('/', $dia);
        $phpdate = mktime($hora, $minuto, 0, $dia[0], $dia[1], $dia[2]);
        return date('Y-m-d H:i:s', $phpdate);
    }

    public static function IntervaloDataNome($data1, $usarSoHojeOntem = 0) {
        $data2 = date("d/m/Y H:i:s");

        for ($i = 1; $i <= 2; $i++) {
            ${"dia" . $i} = substr(${"data" . $i}, 0, 2);
            ${"mes" . $i} = substr(${"data" . $i}, 3, 2);
            ${"ano" . $i} = substr(${"data" . $i}, 6, 4);
            ${"ano" . $i} = substr(${"data" . $i}, 6, 4);
            ${"horas" . $i} = substr(${"data" . $i}, 11, 2);
            ${"minutos" . $i} = substr(${"data" . $i}, 14, 2);
        }

        $segundos = mktime($horas2, $minutos2, 0, $mes2, $dia2, $ano2) - mktime($horas1, $minutos1, 0, $mes1, $dia1, $ano1);
        $difere = round($segundos / 86400);

        //echo "segundos dividido-".((($segundos/60)/60)/24).  "segundos -".$segundos ;

        if ($segundos <= 300) {
            return "quase agora";
        } else {
            if ($segundos <= 600) {
                return "a menos de 10 minutos";
            } else {
                if ($segundos <= 1200) {
                    return "a menos de 20 minutos";
                } else {
                    if ($segundos <= 1800) {
                        return "a menos de 25 minutos";
                    } else {
                        if ($segundos <= 2400) {
                            return "a menos de 30 minutos";
                        } else {
                            if ($segundos <= 3000) {
                                return "a menos de 50 minutos";
                            } else {
                                if ($segundos <= 3600) {
                                    return "a aproximadamente 1 hora";
                                } else {
                                    if ($segundos <= 7200) {
                                        return "~= 2 horas";
                                    } else {
                                        if ($segundos <= 10800) {
                                            return "~= 3 horas";
                                        } else {
                                            if ($segundos <= 14400) {
                                                return "~= 4 horas";
                                            } else {
                                                if ($segundos <= 18000) {
                                                    return "~= 5 horas";
                                                } else {
                                                    if ($segundos <= 18000) {
                                                        return "~= 6 horas";
                                                    } else {
                                                        if (($segundos >= 18000) && ($segundos <= 43200)) {
                                                            return "a mais de 6 horas";
                                                        } else {
                                                            if (($segundos > 43200) && ($segundos <= 86400)) {
                                                                return "a mais de 12 horas";
                                                            } else {
                                                                if (($segundos > 86400) && ($segundos <= 172800)) {
                                                                    return "a mais de 1 dia atrás";
                                                                } else {
                                                                    if (($segundos > 172800) && ($segundos <= 259200)) {
                                                                        return "a mais de 2 dia atrás";
                                                                    } else {
                                                                        if (($segundos > 259200) && ($segundos <= 604800)) {
                                                                            return "a mais de 3 dia atrás";
                                                                        } else {
                                                                            if (($segundos > 604800) && ($segundos <= 1209600)) {
                                                                                return "a mais de 1 semana atrás";
                                                                            } else {
                                                                                if (($segundos > 1209600) && ($segundos <= 1209600)) {
                                                                                    return "neste mês";
                                                                                } else {
                                                                                    if ($segundos > 2417200) {
                                                                                        return "a mais de 1 mês atrás";
                                                                                    }

                                                                                }

                                                                            }

                                                                        }

                                                                    }

                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /*if ($difere == 0) {
         return "Hoje";
         }
         if ($difere == 1) {
         return "Ontem";
         }

         if ($usarSoHojeOntem == 1) {
         return $data1;
         }

         if ($difere < 7) {
         return $difere . " dias atrás";
         } else {
         if ($difere < 30) {
         return round($difere / 7) . " semana(s) atrás";
         } else {
         if ($difere < 365) {
         return round($difere / 30) . " mês(es) atrás";
         } else {
         return round($difere / 365) . " ano(s) atrás";
         }
         }
         }*/
    }

    //Formats------------------------------------------------------------------------------------

    /**
     * Check a format (Date,Time,DateTime,Timer)
     *
     * @param string $Data String to analyze
     * @return String format (DATE,TIME,DATETIME,TIMER)
     */
    public static function FormatTest($Data) {
        //Error default format
        $Out = FALSE;

        if (is_integer($Data) and $Data >= 0) {
            //Timer
            $Out = 'TIMER';
        } elseif (substr_count($Data, '-') == 2 AND substr_count($Data, ':') == 2) {
            //Date and Time
            $DataExp = explode(" ", $Data);
            if (count($DataExp) == 2 AND self::FormatTest($DataExp[0]) == 'DATE' AND self::FormatTest($DataExp[1]) == 'TIME') {$Out = "DATETIME";
            }
        } elseif (substr_count($Data, '-') == 2) {
            //Date
            if (preg_match("/^[0-3][0-9]{3}-[0-9]{1,2}-[0-9]{1,2}$/", $Data)) {
                $DataExp = explode("-", $Data);
                if (checkdate($DataExp[1], $DataExp[2], $DataExp[0])) {
                    $Out = "DATE";
                }
            }
        } elseif (substr_count($Data, ':') == 2) {
            //Time
            if (preg_match("/^[0-2]?[0-9]:[0-5]?[0-9]:[0-5]?[0-9]$/", $Data)) {$Out = "TIME";
            }
        }

        return $Out;
    }

    /**
     * Protects the limits and corrected if necessary
     *
     * @param string $Data
     * @param int $DataMin Value Mini
     * @param int $DataMax Value Max
     * @param int $DataDefo Default
     * @param int $Taille Complete with zero till this size
     * @return string
     */
    public static function FormatCorrige($Data, $DataMin, $DataMax, $DataDefo, $Taille) {
        //Valide
        if (!is_numeric($Data) OR $Data < $DataMin OR $Data > $DataMax) {$Data = $DataDefo;
        }

        //Complete by 0
        if (strlen($Data) != $Taille) {$Data = str_pad($Data, $Taille, '0', STR_PAD_LEFT);
        }

        return $Data;
    }

    /**
     * Convert DateGet into a string
     *
     * @param array $DateArray Array with the date and time
     * @param string $Format vue most of option Date()
     * @return string
     */
    public static function Format($DateArray, $Format = "Y-m-d H:i:s") {
        if (is_array($DateArray)) {
            $Out = $Format;
            foreach ($DateArray as $DataNom => $DataVal) {$Out = str_replace($DataNom, $DataVal, $Out);
            }
        } else {
            //Error input format
            $Out = FALSE;
        }

        return $Out;
    }

    /**
     * Converts a date-type 1D,2W in second
     *
     * @param string $Text
     * @return int
     */
    public static function FormatT2S($Text) {
        if (!is_numeric($Text)) {
            $Text = strtoupper(trim($Text));
            $TextVal = $Text + 0;
            $TextMul = substr($Text, strlen($TextVal . ''));

            $Dico['S'] = 1;
            $Dico['M'] = 60;
            $Dico['H'] = 3600;
            $Dico['D'] = 86400;
            $Dico['W'] = 604800;

            $Out = $TextVal * $Dico[$TextMul];
        } else {$Out = abs(round($Text, 0));
        }

        return $Out;
    }

    /**
     * Converts a date-type second in 1D, 2W
     *
     * @param int $Sec date in second
     * @return string
     */
    public static function FormatS2T($Sec) {
        $Dico[604800] = "W";
        $Dico[86400] = "D";
        $Dico[3600] = 'H';
        $Dico[60] = 'M';

        $Sec += 0;
        if (is_integer($Sec)) {
            $Sec = abs($Sec);
            $Out = $Sec;
            foreach ($Dico as $DataKey => $DataVal) {
                if (is_integer($Sec / $DataKey)) {
                    $Out = ($Sec / $DataKey) . $DataVal;
                    break;
                }
            }
        } else {$Out = 0;
        }

        return $Out;
    }

    /**
     * Converts humain date to mysql format
     *
     * @param string $DateTime
     * @param string $Separateur Separator for date
     * @return string
     */
    public static function FormatHumain2Mysql($DateTime, $Separateur = '/') {
        $Out = FALSE;
        $Date = explode($Separateur, $DateTime);
        if (count($Date) == 3) {$Out = $Date[2] . '-' . $Date[1] . '-' . $Date[0];
        }

        return $Out;
    }

    //Other------------------------------------------------------------------------------------

    /**
     * Convert a Date, Time, DateTime, Timer in DateTime cut in a array
     *
     * @param string $DateTime Date|Time|DateTime|Timer to cut
     * @return array
     */
    public static function Get($DateTime) {
        //Initialise
        $Out = array();
        $Out['NULL'] = FALSE;
        //Empty or NULL
        $Out['Y'] = "0000";
        //Year on 4 Char
        $Out['m'] = "00";
        //Month on Char 2
        $Out['d'] = "00";
        //Day on 2 Char
        $Out['H'] = "00";
        //Hour on 2 Char
        $Out['i'] = "00";
        //Minute on 2 Char
        $Out['s'] = "00";
        //Second on 2 Char

        //Pass Timer in DateTime
        if (self::FormatTest($DateTime) == "TIMER") {$DateTime = date("Y-m-d H:i:s", $DateTime);
        }

        //Finds the Format
        $Format = self::FormatTest($DateTime);

        //Cut
        if (!$DateTime OR $DateTime == "NULL" OR $DateTime === NULL OR $DateTime == '0000-00-00' OR $DateTime == '0000-00-00 00:00:00') {
            //Date Null or Empty
            $Out['NULL'] = TRUE;
        } elseif ($Format == "DATE") {
            //Date
            list($Out['Y'], $Out['m'], $Out['d']) = explode("-", trim($DateTime), 3);
        } elseif ($Format == "TIME") {
            //Time
            list($Out['H'], $Out['i'], $Out['s']) = explode(":", trim($DateTime), 3);
        } elseif ($Format == "DATETIME") {
            //Date and Time
            list($DataD, $DataT) = explode(" ", trim($DateTime), 2);
            list($Out['Y'], $Out['m'], $Out['d']) = explode("-", trim($DataD), 3);
            list($Out['H'], $Out['i'], $Out['s']) = explode(":", trim($DataT), 3);
        } else {
            //Unknown
            return FALSE;
        }

        if (!$Out['NULL']) {
            //Protects the limits and correction
            $Out['Y'] = self::FormatCorrige($Out['Y'], 1, 3000, "0001", 4);
            $Out['m'] = self::FormatCorrige($Out['m'], 1, 12, "01", 2);
            $Out['d'] = self::FormatCorrige($Out['d'], 1, 31, "01", 2);
            $Out['H'] = self::FormatCorrige($Out['H'], 0, 23, "00", 2);
            $Out['i'] = self::FormatCorrige($Out['i'], 0, 59, "00", 2);
            $Out['s'] = self::FormatCorrige($Out['s'], 0, 59, "00", 2);

            //Adding extension
            $Out['a'] = ($Out['H'] > 11) ? 'pm' : 'am';
            $Out['w'] = JDDayOfWeek(cal_to_jd(CAL_GREGORIAN, $Out['m'], $Out['d'], $Out['Y']), 0);
            $Out['g'] = $Out['H'] % 13 + 1;
            $Out['h'] = str_pad($Out['g'], 2, '0', STR_PAD_LEFT);
            $Out['y'] = substr($Out['Y'], 2, 2);
            $Out['X'] = $Out['Y'] . $Out['m'] . $Out['d'];
        }

        return $Out;
    }

    /**
     * Retourn the date format GMT
     *
     * @param int $Timer Number of seconds befor 1970-01-01 00:00:00
     * @return string
     */
    public static function GMT($Timer = NULL) {
        if (is_null($Timer)) {$Out = gmdate('D, d M Y H:i:s') . ' GMT';
        } else {$Out = gmdate('D, d M Y H:i:s', $Timer) . ' GMT';
        }

        return $Out;
    }

    /**
     * Returning the curentlty DateTime
     *
     * @param bool $Timer If true then this is the return Timestamp which is a place of the DateTime
     * @return string/int
     */
    public static function Now($Timer = FALSE) {
        if ($Timer) {$Out = time();
        } else {$Out = date("Y-m-d H:i:s");
        }

        return $Out;
    }

    /**
     * Finds that the date will be in X days
     *
     * @param date/datetime $DateDebut Date of begin
     * @param int $Jours Number of days to adding (if negative, it forward)
     * @param bool $FinDuMois With a seat at the end of the ending month
     * @param bool $FormatDateTime return the date and time
     * @return date/datetime
     */
    public static function DansXJous($DateDebut, $Jours, $FinDuMois = FALSE, $FormatDateTime = FALSE) {
        //Complete with the curently date
        if (is_null($DateDebut)) {$DateDebut = self::Now();
        }

        //Cutting
        if (!is_array($DateDebut)) {$DateDebut = self::Get($DateDebut);
        }

        //Years
        while ($Jours >= self::NbJoursAnnee($DateDebut['Y'])) {
            $Jours -= self::NbJoursAnnee($DateDebut['Y']);
            $DateDebut['Y']++;
        }

        //Month
        while ($Jours >= self::NbJoursMois($DateDebut['Y'], $DateDebut['m'])) {
            $Jours -= self::NbJoursMois($DateDebut['Y'], $DateDebut['m']);
            $DateDebut['m']++;
            if ($DateDebut['m'] > 12) {
                $DateDebut['m'] = 1;
                $DateDebut['Y']++;
            }
        }

        //Days
        $DateDebut['d'] += $Jours;
        if ($DateDebut['d'] > self::NbJoursMois($DateDebut['Y'], $DateDebut['m'])) {
            $DateDebut['d'] -= self::NbJoursMois($DateDebut['Y'], $DateDebut['m']);
            $DateDebut['m']++;
            if ($DateDebut['m'] > 12) {
                $DateDebut['m'] = 1;
                $DateDebut['Y']++;
            }
        }

        //With a seat at the end of the ending month
        if ($FinDuMois) {$DateDebut['d'] = self::NbJoursMois($DateDebut['Y'], $DateDebut['m']);
        }

        //Complete the early 0
        $DateDebut['m'] = str_pad($DateDebut['m'], 2, '0', STR_PAD_LEFT);
        $DateDebut['d'] = str_pad($DateDebut['d'], 2, '0', STR_PAD_LEFT);

        //Return the date and time
        if ($FormatDateTime) {$Out = self::Format($DateDebut, "Y-m-d H:i:s");
        } else {$Out = self::Format($DateDebut, "Y-m-d");
        }

        return $Out;
    }

    /**
     * Finds that the date will be in X months
     *
     * @param date/datetime $DateDebut Date of begin
     * @param int $Mois Number of months to adding (if negative, it forward)
     * @param bool $FinDuMois With a seat at the end of the ending month
     * @param bool $FormatDateTime return the date and time
     * @return date/datetime
     */
    public static function DansXMois($DateDebut, $Mois, $FinDuMois = FALSE, $FormatDateTime = FALSE) {
        //Complete with curently date
        if (is_null($DateDebut)) {$DateDebut = self::Now();
        }

        //Cutting
        if (!is_array($DateDebut)) {$DateDebut = self::Get($DateDebut);
        }

        //Years
        while ($Mois >= 12) {
            $Mois -= 12;
            $DateDebut['Y']++;
        }

        //Month
        $DateDebut['m'] += $Mois;
        if ($DateDebut['m'] > 12) {
            $DateDebut['m'] -= 12;
            $DateDebut['Y']++;
        }

        //With a seat at the end of the ending month
        if ($FinDuMois) {$DateDebut['d'] = self::NbJoursMois($DateDebut['Y'], $DateDebut['m']);
        }

        //Complete les 0 au début
        $DateDebut['m'] = str_pad($DateDebut['m'], 2, '0', STR_PAD_LEFT);
        $DateDebut['d'] = str_pad($DateDebut['d'], 2, '0', STR_PAD_LEFT);

        //return the date and time
        if ($FormatDateTime) {$Out = self::Format($DateDebut, "Y-m-d H:i:s");
        } else {$Out = self::Format($DateDebut, "Y-m-d");
        }

        return $Out;
    }

    //Duration------------------------------------------------------------------------------------

    /**
     * Detect if a leap year
     *
     * @param int $Annee Year to test
     * @return bool
     */
    public static function IsBissextile($Annee) {
        if ($Annee % 4 == 0 AND ($Annee % 100 != 0 OR $Annee % 400 == 0)) {$Out = TRUE;
        } else {$Out = FALSE;
        }

        return $Out;
    }

    /**
     * Number of days in a year
     *
     * @param int $Annee Year to test (or a date)
     * @return int
     */
    public static function NbJoursAnnee($Annee) {
        //It is a date, so I extracted the year
        if (!is_numeric($Annee)) {
            $Date = self::Get($Annee);
            $Annee = $Date['Y'];
        }

        //leap year
        if (self::IsBissextile($Annee)) {$Out = 366;
        } else {$Out = 365;
        }

        return $Out;
    }

    /**
     * Number of days in a month
     *
     * @param int $Annee Year to test (or a date)
     * @param int $Mois Month to test
     * @return int
     */
    public static function NbJoursMois($Annee, $Mois) {
        //It is a date, so I extracted the year
        if (!is_numeric($Annee)) {
            $Date = self::Get($Annee);
            $Annee = $Date['Y'];
            $Mois = $Date['m'];
        }
        //Eliminates 0 and force passage digital
        $Mois += 0;

        $MoisJours[1] = 31;
        $MoisJours[2] = 28;
        $MoisJours[3] = 31;
        $MoisJours[4] = 30;
        $MoisJours[5] = 31;
        $MoisJours[6] = 30;
        $MoisJours[7] = 31;
        $MoisJours[8] = 31;
        $MoisJours[9] = 30;
        $MoisJours[10] = 31;
        $MoisJours[11] = 30;
        $MoisJours[12] = 31;

        //Correction possible for leap year
        if ($Mois == 2) {
            if (self::IsBissextile($Annee)) {$MoisJours[2]++;
            }
        }

        //Find the number of days in a month
        $Out = $MoisJours[$Mois];

        return $Out;
    }

    //Unlike------------------------------------------------------------------------------------

    /**
     * Find the difference between 2 dates
     * The negative result means that the dates are in line with decreasing
     *
     * @param date $DateDebut Date of begin
     * @param date $DateFin Date of end
     * @return array (0 or Y = Year, or 1 m = Month, or d = 2 Days)
     */
    public static function DiffDate($DateDebut, $DateFin = NULL) {
        //Complete with curently date
        if (is_null($DateFin)) {$DateFin = self::Now();
        }

        //cutting
        if (!is_array($DateDebut)) {$DateDebut = self::Get($DateDebut);
        }
        if (!is_array($DateFin)) {$DateFin = self::Get($DateFin);
        }

        //Met dates in the correct order
        if ($DateDebut['X'] > $DateFin['X']) {
            core::Swap($DateDebut, $DateFin);
            $Signe = -1;
        } else {$Signe = 1;
        }

        //Numerical Approach
        $Annees = $DateFin['Y'] - $DateDebut['Y'];
        $Mois = $DateFin['m'] - $DateDebut['m'];
        $Jours = $DateFin['d'] - $DateDebut['d'];

        //Fix by the day
        if ($Jours < 0) {
            $Mois--;
            $Jours += self::NbJoursMois($DateDebut['Y'], $DateDebut['m']);
        }
        $JoursMax = self::NbJoursMois($DateFin['Y'], $DateFin['m']);
        if ($Jours >= $JoursMax) {
            $Jours -= $JoursMax;
            $Mois++;
        }

        //Fix by the month
        if ($Mois < 0) {
            $Annees--;
            $Mois += 12;
        }
        if ($Mois >= 12) {
            $Mois -= 12;
            $Annees++;
        }

        $Out = array($Signe * $Annees, $Signe * $Mois, $Signe * $Jours);
        return $Out;
    }

    /**
     * Find out how many years between 2 dates
     * The negative result means that the dates are in line with decreasing
     *
     * @param date $DateDebut Date of begin
     * @param date $DateFin Date of end
     * @return int
     */
    public static function DiffAnnee($DateDebut, $DateFin = NULL) {
        $Diff = self::DiffDate($DateDebut, $DateFin);

        return $Diff[0];
    }

    /**
     * Find out how many months between 2 dates
     * The negative result means that the dates are in line with decreasing
     *
     * @param date $DateDebut Date of begin
     * @param date $DateFin Date of end
     * @return int
     */
    public static function DiffMois($DateDebut, $DateFin = NULL) {
        $Diff = self::DiffDate($DateDebut, $DateFin);
        $Mois = $Diff[0] * 12 + $Diff[1];

        return $Mois;
    }

    /**
     * Find out how many days between 2 dates
     * The negative result means that the dates are in line with decreasing
     *
     * @param date $DateDebut Date of begin
     * @param date $DateFin Date of end
     * @return int
     */
    public static function DiffJours($DateDebut, $DateFin = NULL) {
        //complet with date this
        if (is_null($DateFin)) {$DateFin = self::Now();
        }

        //cutting
        if (!is_array($DateDebut)) {$DateDebut = self::Get($DateDebut);
        }
        if (!is_array($DateFin)) {$DateFin = self::Get($DateFin);
        }

        //Met dates in the correct order
        if ($DateDebut['X'] > $DateFin['X']) {
            core::Swap($DateDebut, $DateFin);
            $Signe = -1;
        } else {$Signe = 1;
        }

        //Determine whether it is the same month in the same year
        if ($DateFin['Y'] == $DateDebut['Y'] AND $DateFin['m'] == $DateDebut['m']) {$Jours = $DateFin['d'] - $DateDebut['d'];
        } else {
            //Number of days until the end of the month
            $Jours = self::NbJoursMois($DateDebut['Y'], $DateDebut['m']) - $DateDebut['d'];

            //Number of days at the end
            $Jours += $DateFin['d'];

            //Determine whether it's the same year
            if ($DateFin['Y'] == $DateDebut['Y']) {
                for ($i = $DateDebut['m'] + 1; $i < $DateFin['m']; $i++) {$Jours += self::NbJoursMois($DateDebut['Y'], $i);
                }
            } else {
                //Number of days until the end of the year
                for ($i = $DateDebut['m'] + 1; $i < 13; $i++) {$Jours += self::NbJoursMois($DateDebut['Y'], $i);
                }

                //Number of days at the end
                for ($i = 1; $i < $DateFin['m']; $i++) {$Jours += self::NbJoursMois($DateFin['Y'], $i);
                }

                //Number of days in years
                for ($i = $DateDebut['Y'] + 1; $i < $DateFin['Y']; $i++) {$Jours += self::NbJoursAnnee($i);
                }
            }
        }

        $Out = $Signe * $Jours;
        return $Out;
    }

    public static function ExtensoMes($mes){
        if($mes == 01){
           $extensoMes = "Jan";
        }elseif ($mes ==02){
           $extensoMes = "Fev";
        }elseif ($mes ==03){
           $extensoMes = "Mar";
        }elseif ($mes ==04){
           $extensoMes = "Abr";
        }elseif ($mes ==05){
           $extensoMes = "Mai";
        }elseif ($mes ==06){
           $extensoMes = "Jun";
        }elseif ($mes ==07){
           $extensoMes = "Jul";
        }elseif ($mes ==08){
           $extensoMes = "Ago";
        }elseif ($mes ==09){
           $extensoMes = "Set";
        }elseif ($mes ==10){
           $extensoMes = "Out";
        }elseif ($mes ==11){
           $extensoMes = "Nov";
        }elseif ($mes ==12){
           $extensoMes = "Dez";
        }

        return $extensoMes;
    }

}
?>
