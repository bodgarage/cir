<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;

use app\models\Pessoas;
use app\models\TiposPessoas;
use app\models\Monitor;
use app\models\Controles;
use app\models\Canais;
use app\models\Logs;

use \lithium\security\Auth;
use \lithium\storage\Session;
use app\controllers\Images;
use lithium\core\Environment;
use lithium\util\String;

class CanaisController extends \lithium\action\Controller {

    protected function _init() {
        $this -> _render['negotiate'] = true;
        parent::_init();

    }

    public function render($options = array()) {

        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }

    public function index() {

        if (Auth::check('member')) {

            $conditions = array('id' => Session::read('member.id'), 'sn_excluido' => 'False');
            $pessoa = Pessoas::find('first', array('conditions' => $conditions));
            if(($pessoa->tipo_pessoa_id==1)||($pessoa->tipo_pessoa_id==2)){
                $canais = Canais::all(array('with'=>array('Pessoas','Controles')));
                $canais = $canais->to('array');
                //print_r($pessoas);exit;
                // talvez exista como fazer o lithium retornar como array



            }else{
                $this -> redirect('/pessoas/permissoes/');
            }

        } else {
            $this -> redirect('/pessoas/login/');
        }
        return compact('canais');

    }

    public function add() {
        $register = NULL;

        //echo "entrei";

        if (Auth::check('member')) {
            //echo "entrei 2";

            //verifica se é administrador ou marketing
            if(Session::read('member.tipo_pessoa_id')==1){
                if ($this -> request -> data) {
                    $this -> request -> data['pessoa_id']=Session::read('member.id');
                    $register = Canais::create($this -> request -> data);
                    if ($register -> save()) {
                        //print_r($register);
                        $log['pessoa_id'] = Session::read('member.id');
                        $log['acao_id'] = Logs::$_ADDCANAL;
                        $log['ip'] = $this->request->env('REMOTE_ADDR');
                        $log['data']= date("Y-m-d H:i:s");
                        $newlog = Logs::create($log);
                        if(!$newlog->save()){
                            echo "Falha ao gravar o Log";
                        }
                        $this -> redirect('/canais/');
                    }
                }
                $fields = array('id','descricao', 'hardware');
                $controles = Controles::all(array('fields'=>$fields,'with'=>array('Pessoas')));
                $controles = $controles->to('array');
                foreach($controles as $key => $controle){
                    $aux[$key]=$controle['descricao'].' - '.$controle['hardware'];

                }

                $controles=$aux;


            }else{
                $this -> redirect('/pessoas/permissoes');
            }
        } else {
            $this -> redirect('/pessoas/login/');
        }

        $data = $this -> request -> data;

        return compact('data', 'controles');

    }

    public function edit($id=null) {

    }

    public function remove($id) {

    }

    public function view() {

        $pessoa = NULL;

        if (Auth::check('member')) {
//echo "entrei";
            $conditions = array('id' => Session::read('member.id'), 'sn_excluido' => 'False');
            $pessoa = Pessoas::find('first', array('conditions' => $conditions));

        } else {
            $this -> redirect('/pessoas/login/');
        }
        //print_r($pessoa);
        return compact('pessoa');

    }



}
?>
