<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;

use app\models\Pessoas;
use app\models\TiposPessoas;
use app\models\Monitor;
use app\models\Logs;

use \lithium\security\Auth;
use \lithium\storage\Session;
use app\controllers\Images;
use lithium\core\Environment;
use lithium\util\String;
use lithium\action\Request;
use lithium\util\Validator;

class PessoasController extends \lithium\action\Controller {

	protected function _init() {
		$this -> _render['negotiate'] = true;
		parent::_init();

	}


    public function render($options = array()) {

        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }


	public function index() {

		if (Auth::check('member')) {


			if((Session::read('member.tipo_pessoa_id')==1) || (Session::read('member.tipo_pessoa_id')==2)){
				$pessoas = Pessoas::all();
                $pessoas = $pessoas->to('array');
                //print_r($pessoas);exit;
				// talvez exista como fazer o lithium retornar como array
				$conditions = array('sn_excluido' => 'False');
				$tiposPessoas = TiposPessoas::find('all', array('conditions' => $conditions));
	            $tiposPessoas = $tiposPessoas->to('array');
				//talvez este laço pode dar pau quando uma midia for excluida e gerar relacionamento errado


			}else{
				$this -> redirect('/pessoas/permissoes/');
			}

		} else {
			$this -> redirect('/pessoas/login/');
		}
		return compact('tiposPessoas', 'pessoas');

	}


	public function add() {
		$register = NULL;

        //echo "entrei";
        //print_r($this->request->env('REMOTE_ADDR'));exit;

		if (Auth::check('member')) {
		    //echo "entrei 2";


			//verifica se é administrador
			if(Session::read('member.tipo_pessoa_id')==1){
				if ($this -> request -> data) {
					$register = Pessoas::create($this -> request -> data);
					if ($register -> save()) {
					    //print_r($register);
					    $log['pessoa_id'] = Session::read('member.id');
                        $log['acao_id'] = Logs::$_ADDUSER;
                        $log['ip'] = $this->request->env('REMOTE_ADDR');
                        $log['data']= date("Y-m-d H:i:s");

                        $newlog = Logs::create($log);
                        if(!$newlog->save()){
                            echo "Falha ao gravar o Log";
                        }


						$this -> redirect('/pessoas/');
					}
				}


				$conditions = array('sn_excluido' => 'False');
				$tiposPessoas = TiposPessoas::find('all', array('conditions' => $conditions));
	            $tiposPessoas = $tiposPessoas->to('array');
				foreach ($tiposPessoas as $key => $tipopessoa){
				    $aux[$key]=$tipopessoa['tipoPessoa'];
                    //print_r($tipopessoa);
				}
                //print_r($aux);
	            $tiposPessoas = $aux;

			}else{
				$this -> redirect('/pessoas/permissoes/');
			}
		} else {
			$this -> redirect('/pessoas/login/');
		}

		$data = $this -> request -> data;

		return compact( 'data', 'pessoa', 'tiposPessoas');

	}


	public function edit($id=null) {

	}


	public function remove($id) {

	}


	public function view() {

		$pessoa = NULL;

		if (Auth::check('member')) {
//echo "entrei";
			$conditions = array('id' => Session::read('member.id'), 'sn_excluido' => 'False');
			$pessoa = Pessoas::find('first', array('conditions' => $conditions));

		} else {
			$this -> redirect('/pessoas/login/');
		}
        //print_r($pessoa);
		return compact('pessoa');

	}


	public function login() {
		//assume there's no problem with authentication
		//$noauth = false;
		//perform the authentication check and redirect on success
		//if theres still post data, and we werent redirected above, then login failed

		//Return noauth status
		//if (Auth::check('member')){
		//    $this -> redirect('/pessoas/view/');
		//}
		if (!empty($this -> request -> data)) {

			if (Auth::check('member', $this -> request)) {
			    $log['pessoa_id'] = Session::read('member.id');
                $log['acao_id'] = Logs::$_LOGIN;
                $log['ip'] = $this->request->env('REMOTE_ADDR');
                $log['data']= date("Y-m-d H:i:s");
                $newlog = Logs::create($log);
                if(!$newlog->save()){
                    echo "Falha ao gravar o Log";
                }

				$this -> redirect('/pessoas/view/');
			}
			if ($this -> request -> data) {
				//Login failed, trigger the error message
				$noauth = true;
			}
		}

		//return compact('noauth');
	}


	public function logout() {
		Auth::clear('member');
		return $this -> redirect('/');
	}


    public function permissoes(){

    }

    public function passwordchange() {
        if (Auth::check('member')) {
            $conditions = array('id'=>Session::read('member.id'));
            $user = Pessoas::find('first',array('conditions'=>$conditions));

            if ($this -> request -> data) {

                $rules = array('newpassword1' => array( array('notEmpty', 'message' => 'A nova senha não pode ser em branco')));
                $data = array('newpassword1' => $this -> request -> data['newpassword1']);
                $errors = Validator::check($data, $rules);
                if (!$errors) {

                    if ($this -> request -> data['newpassword1'] == $this -> request -> data['newpassword2']) {

                        if ($user -> senha == \lithium\util\String::hash($this -> request -> data['senha'])) {

                            $user -> senha = \lithium\util\String::hash($this -> request -> data['newpassword1']);

                            if ($user -> save()) {
                                $errors['pessoa'][1] = 'Senha alterada com sucesso';
                            } else {
                                $user = null;
                                $errors['pessoa'][1] = 'Não foi possivel redefinir sua senha';
                            }

                        } else {
                            $errors['pessoa'][0] = 'Sua senha atual não confere.';
                        }
                    } else {
                        $errors['pessoa'][2] = 'A nova senha difere da confirmação de nova senha';

                    }
                }
            }
        } else {
            $this -> redirect('/' . Environment::get('locale') . '/pessoas/login/');
        }

        return compact('errors');
    }

}
?>
