<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\controllers;

use app\models\Pessoas;
use app\models\TiposPessoas;
use app\models\Monitor;
use app\models\Controles;
use app\models\Canais;
use app\models\Tarefas;
use app\models\Logs;

use \lithium\security\Auth;
use \lithium\storage\Session;
use app\controllers\Images;
use lithium\core\Environment;
use lithium\util\String;

class TarefasController extends \lithium\action\Controller {

    protected function _init() {
        $this -> _render['negotiate'] = true;
        parent::_init();

    }

    public function render($options = array()) {

        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }

    public function index($page=1, $limit=20) {

        if (Auth::check('member')) {


            if((Session::read('member.tipo_pessoa_id')==1)||(Session::read('member.tipo_pessoa_id')==2)){
                //$order = array('data'=>'DESC');
                $order=array();
                $tarefas = Tarefas::all(array('with'=>array('Pessoas','Canais'), 'order'=>$order ,'page'=>$page, 'limit' => $limit));
                $tarefas = $tarefas->to('array');

                foreach ($tarefas as $key => $tarefa) {
                    $conditions = array('id'=>$tarefa['canai']['controle_id']);
                    $controle = Controles::find('first', array('conditions'=>$conditions));
                    $controle = $controle->to('array');
                    $tarefas[$key][canai]['controle']=$controle;
                }

                //print_r($tarefas);exit;
                // talvez exista como fazer o lithium retornar como array


            }else{
                $this -> redirect('/pessoas/permissoes/');
            }

        } else {
            $this -> redirect('/pessoas/login/');
        }
        return compact('tarefas','page','limit');

    }

    public function add() {
        $register = NULL;

        //echo "entrei";

        if (Auth::check('member')) {
            //echo "entrei 2";

            //verifica se é administrador
            if(Session::read('member.tipo_pessoa_id')==1){
                if ($this -> request -> data) {
                    $this -> request -> data['pessoa_id']=Session::read('member.id');
                    $register = Tarefas::create($this -> request -> data);
                    if ($register -> save()) {
                        //print_r($register);
                        $log['pessoa_id'] = Session::read('member.id');
                        $log['acao_id'] = Logs::$_ADDTAREFA;
                        $log['ip'] = $this->request->env('REMOTE_ADDR');
                        $log['data']= date("Y-m-d H:i:s");
                        $newlog = Tarefas::create($log);
                        if(!$newlog->save()){
                            echo "Falha ao gravar o Log";
                        }
                        $this -> redirect('/tarefas/');
                    }
                }

                $canais = Canais::all();
                $canais = $canais->to('array');
                foreach($canais as $key => $canal){
                    $aux[$key]=$canal['descricao'].' - gpio'.$canal['canal'];

                }

                $canais=$aux;

            }else{
                $this -> redirect('/pessoas/permissoes');
            }
        } else {
            $this -> redirect('/pessoas/login/');
        }

        $data = $this -> request -> data;

        return compact('register','data', 'canais');

    }

    public function edit($id=null) {

    }

    public function remove($id) {

    }

    public function view() {


    }



}
?>
