<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

namespace app\controllers;

use app\models\Pessoas;


use \lithium\security\Auth;
use lithium\core\Environment;
use \lithium\storage\Session;
use lithium\core\ConfigException;
use lithium\action\Request;




/**
 * This controller is used for serving static pages by name, which are located in the `/views/pages`
 * folder.
 *
 * A Lithium application's default routing provides for automatically routing and rendering
 * static pages using this controller. The default route (`/`) will render the `home` template, as
 * specified in the `view()` action.
 *
 * Additionally, any other static templates in `/views/pages` can be called by name in the URL. For
 * example, browsing to `/pages/about` will render `/views/pages/about.html.php`, if it exists.
 *
 * Templates can be nested within directories as well, which will automatically be accounted for.
 * For example, browsing to `/pages/about/company` will render
 * `/views/pages/about/company.html.php`.
 */
class PagesController extends \lithium\action\Controller {

    protected function _init() {
        $this->_render['negotiate'] = true;
        parent::_init();
    }

    public function render($options = array()) {
        $page = TRUE;
        $this -> set(compact('page'));
        $user = Pessoas::getPessoa();
        $this -> set(compact('user'));

        parent::render($options);
    }

	public function view() {
		$path = func_get_args() ?: array('home');

		return $this->render(array('template' => join('/', $path)));
	}
}

?>
