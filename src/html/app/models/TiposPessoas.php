<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class TiposPessoas extends \lithium\data\Model {

	public $validates = array(

    'tipoPessoa' => array( array('notEmpty', 'message' => 'É preciso definir uma descrição do tipo')),

	 );

	protected function _init() {
        $this->_render['negotiate'] = true;
        parent::_init();
    }

}

