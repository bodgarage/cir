<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Controles extends \lithium\data\Model {



    public $belongsTo = array('Pessoas');

    public $hasMany = array('Canais');

    public $validates = array(
       'hardware' => array(
           array('notEmpty', 'message' => 'insira um número do canal, ex.: gpio16 = canal 16'),
       ),
       'endereco' => array(
           array('notEmpty', 'message' => 'insira o endereço remoto, ex.: gpio16 = canal 16'),
       ),
       'porta' => array(
           array('notEmpty', 'message' => 'insira a porta, ex.: gpio16 = canal 16'),
       ),
     );

    public static function setUp($id=null){

            if(is_numeric($id)){

                $conditions = array('id'=>$id);
                $controles = Controles::find('first',array('conditions'=>$conditions, 'with'=>array('Pessoas','Canais')));
                $controles = $controles->to('array');
                //print_r($canal);exit;
                if($controles['hardware']=='raspberry'){
                    //$leituras = Monitor::getStatus($canal['controle']['id']);
                    //print_r($leituras);exit;
                   /* if($canal['logicaInvertida'] == "True"){
                        if($leituras[$canal['canal']][0]==0){
                           exec('echo "0" > /sys/class/gpio/gpio'.$canal['canal'].'/value', $atuar, $retval);
                           $log['status']='0';
                        }else{
                           exec('echo "1" > /sys/class/gpio/gpio'.$canal['canal'].'/value', $atuar, $retval);
                           $log['status']='1';
                        }
                    }else{
                        if($leituras[$canal['canal']][0]==0){
                           exec('echo "1" > /sys/class/gpio/gpio'.$canal['canal'].'/value', $atuar, $retval);
                           $log['status']='1';
                        }else{
                           exec('echo "0" > /sys/class/gpio/gpio'.$canal['canal'].'/value', $atuar, $retval);
                           $log['status']='0';
                        }
                    }*/

                }elseif($controles['hardware']=='arduino'){

                      //enviar requisição para arduino

                      $log['status']=Canais::setUpRemoteArduino($canal['id'], 'True');

                }
            }


        //$leituras = Monitor::getStatus($canal['controle']['id']);
        //print_r($leituras);
        //$this -> set(compact('leituras'));
        //$this->render(array('template' => 'index'));

        //return compact('leituras');
    }


}


?>
