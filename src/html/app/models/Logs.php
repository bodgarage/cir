<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Logs extends \lithium\data\Model {

    public static $_LOGIN                = 0;
    public static $_ADDUSER              = 1;  // adicionou novo usuário
    public static $_ADDCONTROL           = 2;  // adicionou novo controle
    public static $_ADDCANAL             = 3;
    public static $_ADDTAREFA            = 4;  //
    public static $_CHANGESTATUS         = 5;  //
    public static $_MONITOR              = 6;  //
    public static $_MONITORCHANGESTATUS  = 7;
    public static $_SERVERDOWN           = 8;

    public $belongsTo = array('Pessoas','Canais');

    public $validates = array(
       'pessoa_id' => array(
           array('notEmpty', 'message' => 'insira um número do canal, ex.: gpio16 = canal 16'),
       ),
     );


}


?>
