<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use app\models\Controles;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Monitor extends \lithium\data\Model {


    public static function getStatus($controle_id) {
        $leituras = array();
        $conditions=array('controle_id'=>$controle_id, 'sn_excluido'=>'False');
        $canais = Canais::find('all',array('conditions'=>$conditions,'with'=>'Controles'));
        $canais = $canais->to('array');
        //print_r($canais);exit;
        //$canais = array('16'=>'gpio16','20'=>'gpio20','21'=>'gpio21',);

        foreach($canais as $key => $canal) {
            //print_r($key);
            //print_r($canal);
            $retArr=null;
            $retVal=null;
            if($canal['controle']['hardware']=='raspberry'){
                exec('cat /sys/class/gpio/gpio'.$canal['canal'].'/value', $retArr, $retVal);

                if($canal['logicaInvertida'] == "True"){

                    if($retArr[0]==0){
                       $retArr[0]=1;
                    }elseif($retArr[0]==1){
                       $retArr[0]=0;
                    }
                }
                //print_r($retArr);
                //print_r($retVal);
                $leituras[$canal['canal']][]=$retArr[0];
                $leituras[$canal['canal']][]=$canal;
                $leituras[$canal['canal']][]=$key;
            }elseif($canal['controle']['hardware']=='arduino'){
                //solicitar alteração de status

                $leituras[$canal['canal']][]=Canais::setRemoteArduino($key);
                $leituras[$canal['canal']][]=$canal;
                $leituras[$canal['canal']][]=$key;
            }

            //array_push($leituras, $leitura);

        }
        //print_r($leituras);exit;
        return $leituras;
    }

}


?>
