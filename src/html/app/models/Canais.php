<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Canais extends \lithium\data\Model {



    public $belongsTo = array('Pessoas', 'Controles');

    public $validates = array(
       'canal' => array(
           array('notEmpty', 'message' => 'insira um número do canal, ex.: gpio16 = canal = 16'),
       ),
       'controle_id' => array(
           array('notEmpty', 'message' => 'insira um número do canal, ex.: gpio16 = canal = 16'),
       ),
     );

    public static function isJSON($string){
        return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

     public static function set($id=null){

            if(is_numeric($id)){

                $conditions = array('id'=>$id);
                $canal = Canais::find('first',array('conditions'=>$conditions, 'with'=>array('Pessoas','Controles')));
                $canal = $canal->to('array');
                //print_r($canal);exit;
                if($canal['controle']['hardware']=='raspberry'){
                    $leituras = Monitor::getStatus($canal['controle']['id']);
                    //print_r($leituras);exit;
                    if($canal['logicaInvertida'] == "True"){
                        if($leituras[$canal['canal']][0]==0){
                           // echo "passei 1";

                           exec("sudo sh -c  'echo \"0\" > /sys/class/gpio/gpio".$canal['canal']."/value'", $atuar, $retval);
                           $log['status']='0';
                        }else{
                            //echo "passei 2";
                           exec("sudo sh -c  'echo \"1\" > /sys/class/gpio/gpio".$canal['canal']."/value'", $atuar, $retval);
                           $log['status']='1';
                        }
                        //echo "passei 3";
                        //echo "sudo sh -c  'echo \"1\" > /sys/class/gpio/gpio".$canal['canal']."/value'";
                        //print_r($atuar);
                        //print_r($retval);
                        //exit;

                    }else{
                        if($leituras[$canal['canal']][0]==0){
                           exec("sudo sh -c  'echo \"1\" > /sys/class/gpio/gpio".$canal['canal']."/value'", $atuar, $retval);
                           $log['status']='1';
                        }else{
                           exec("sudo sh -c  'echo \"0\" > /sys/class/gpio/gpio".$canal['canal']."/value'", $atuar, $retval);
                           $log['status']='0';
                        }
                    }
                    //$comando ='echo "0" > /sys/class/gpio/gpio'.$canal['canal'].'/value';
                    //print_r($atuar);
                    //print_r($comando);
                    //print_r($retval);
                    //exit;

                }elseif($canal['controle']['hardware']=='arduino'){

                      //enviar requisição para arduino

                      $log['status']=Canais::setRemoteArduino($canal['id'], 'True');

                }
            }


        $leituras = Monitor::getStatus($canal['controle']['id']);
        //print_r($leituras);
        //$this -> set(compact('leituras'));
        //$this->render(array('template' => 'index'));

        return compact('leituras');
    }

    /*

        setRemoteArduino()
        $id = id do registro do canal a ser ajustado
        $set = False caso não seja necessário alterar status/ True caso seja necessário alterar status


    */

    public static function setRemoteArduino($id, $set='False'){

        $conditions = array('id'=>$id);
        $canal = Canais::find('first',array('conditions'=>$conditions, 'with'=>array('Pessoas','Controles')));
        $canal = $canal->to('array');
        //print_r($canal);
        $credentials = "cm9vdDoxMjMxMjMxMjM=";

        //$status=Canais::getRemoteArduino($id);
        $url = "http://".$canal['controle']['endereco'].":".$canal['controle']['porta']."/arduino/digital/".$canal['canal'];
        $page = "/arduino/digital/".$canal['canal'];
        $headers = array(
            "GET ".$page." HTTP/1.0",
            "Content-type: text/html;charset=\"utf-8\"",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            //"SOAPAction: \"run\"",
            //"Content-length: ".strlen($xml_data),
            "Authorization: Basic " . $credentials
        );
        //print_r(base64_encode("root:123123123")); //gere a senha para o yun.
        //base64_encode("root:password");
        //print_r($url);//exit;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

        // Apply the XML to our curl call
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

        $data = curl_exec($ch);


        // tratamento dos dados recebidos de arduino ethernet shield
        if(Canais::isJSON($data)){
           $data = json_decode($data);
           //print_r($data);
           $data = $data->status;
           //echo "sim";
        }else{
           //echo "não";
        }

        $status=trim($data);
        //echo "valor".$data;
        if (curl_errno($ch)) {
            $log['pessoa_id'] = 1;
            $log['acao_id'] = Logs::$_SERVERDOWN;
            $log['ip'] = $canal['controle']['endereco'].":".$canal['controle']['porta'];
            $log['canai_id']=$canal['id'];
            $log['data']= date("Y-m-d H:i:s");
            $newlog = Logs::create($log);
            if(!$newlog->save()){
                echo "Falha ao gravar o Log";
            }
            //print "Error 1: " . curl_error($ch)."\n";
        } else {
            // Show me the result
            $data=$status;
            //print_r($data);
            //print_r($set);exit;
            if($set=='True'){
                $url = "http://". $canal['controle']['endereco'].":". $canal['controle']['porta']."/arduino/digital/". $canal['canal'];
                $page = "/arduino/digital/".$canal['canal'];

                if($status==1){
                    $url.="/0";
                    $page.="/0";
                }elseif($status==0){
                    $url.="/1";
                    $page.="/1";
                }

                //print_r($url);
                //print_r($page);exit;
                $headers = array(
                    "GET ".$page." HTTP/1.0",
                    "Content-type: text/html;charset=\"utf-8\"",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    //"SOAPAction: \"run\"",
                    //"Content-length: ".strlen($xml_data),
                    "Authorization: Basic " . $credentials
                );
               // print_r($url);exit;


                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                //curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

                // Apply the XML to our curl call
                //curl_setopt($ch, CURLOPT_POST, 1);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

                $data = curl_exec($ch);

                $data=trim($data);

                //print_r($data);
                //print_r($set);exit;
                if (curl_errno($ch)) {
                    $log['pessoa_id'] = 1;
                    $log['acao_id'] = Logs::$_SERVERDOWN;
                    //$log['ip'] = $this->request->env('REMOTE_ADDR');
                    $log['data']= date("Y-m-d H:i:s");
                    $newlog = Logs::create($log);
                    if(!$newlog->save()){
                        echo "Falha ao gravar o Log";
                    }
                    //print "Error 2: " . curl_error($ch)."\n";
                } else {
                    // Show me the result

                }


            }
            if($canal['logicaInvertida']=='True'){
                //echo "entrei -" . $data;
                if($data=='0'){
                   $data='1';
                }else{
                   $data='0';
                }
                //echo "sai -" . $data;
            }
        }
        /*if($canal['logicainvertida']=='True'){
            if($status){
                $status=0;
            }else{
                $status=1;
            }
        }*/

        curl_close($ch);
        //print_r($data);
        //print_r($canal);


        return $data;
    }

   /* public static function getRemoteArduino($id){

        $conditions = array('id'=>$id);
        $canal = Canais::find('first',array('conditions'=>$conditions, 'with'=>array('Pessoas','Controles')));
        $canal = $canal->to('array');
        //print_r($canal);
        $credentials = "cm9vdDoxMjNtdWRhcg==";

        $url = "http://".$canal['controle']['endereco'].":".$canal['controle']['porta']."/arduino/digital/".$canal['canal'];
        $page = "/arduino/digital/".$canal['canal'];
        $headers = array(
            "GET ".$page." HTTP/1.0",
            "Content-type: text/html;charset=\"utf-8\"",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            //"SOAPAction: \"run\"",
            //"Content-length: ".strlen($xml_data),
            "Authorization: Basic " . $credentials
        );
       // print_r($url);exit;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

        // Apply the XML to our curl call
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

        $data = curl_exec($ch);

        $data=trim($data);
        if (curl_errno($ch)) {
            print "Error: " . curl_error($ch);
        } else {
            // Show me the result

        }
        curl_close($ch);
        //print_r($data);
       // print_r($canal);
        if($canal['logicainvertida']=='True'){
            //echo "entrei -" . $data;
           if($data=='0'){
              $data='1';
           }else{
              $data='0';
           }
           //echo "sai -" . $data;
        }

        return $data;
    }*/


}


?>
