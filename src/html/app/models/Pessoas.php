<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Pessoas extends \lithium\data\Model {


	public $hasMany = array('Controles');

	public $validates = array(
	   'email' => array(
	       array('notEmpty', 'message' => 'Você precisa definir um e-mail'),
	       array('isUniqueEmail', 'message' => 'E-mail já utilizado'),
	       array('email', 'message' => 'preencha com um e-mail valido'),
       ),
       'tipo_pessoa_id' => array(
           array('notEmpty', 'message' => 'É preciso definir um tipo para esta pessoa'),
  	   ),
       'senha' => array(
           array('notEmpty', 'message' => 'insira uma senha pessoal'),
       ),
	 );

	public static function __init() {
		$self = static::_object();
		static::config();

		$self -> _finders['pessoasdepartamentos'] = function($self, $params, $chain) use (&$query, &$classes) {
			$db = Connections::get($self::meta('connection'));

			$query = "select pessoasDepartamentos.*, pd.pessoa_id, pd.departamento_id, pd.dataVinculo, pd.DataDesvinculo from (select pessoas.id,pessoas.nome, pessoas.email,pessoas.tipo_pessoa_id , departamentos.id as dp_id, departamentos.nomeDepartamento, pessoas.sn_excluido from pessoas, departamentos where pessoas.sn_excluido = 'False' and departamentos.sn_excluido='False') as pessoasDepartamentos left join pessoas_departamentos as pd on pd.pessoa_id = pessoasDepartamentos.id and pd.departamento_id = pessoasDepartamentos.dp_id and pessoasDepartamentos.sn_excluido = 'False' and pd.sn_excluido = 'False' order by pessoasDepartamentos.id, pessoasDepartamentos.dp_id";

			$records = $db -> read($query, array('return' => 'array'));
			return $records;
		};

		Validator::add('isUniqueEmail', function ($value, $format, $options) {
				$conditions = array('email' => $value);

				// If editing the user, skip the current user
				if (isset($options['values']['id'])) {
					$conditions[] = 'id != ' . $options['values']['id'];
				}

				// Lookup for users with same email
				return !Pessoas::find('first', array('conditions' => $conditions));
			});

	}


	public static function getPessoa() {
        $logged = false;
        $user = new Pessoas;
        if(Auth::check('member')){
            if ($user_id = Session::read('member.id')) {
                $logged = true;
                $login_tipo = 'member';
            }

        }
        if ($logged) {
            $fields = array('Pessoas.id','Pessoas.nome', 'Pessoas.imagem_perfil', 'Pessoas.senha'
                      );
            $user = Pessoas::find('first', array('fields'=>$fields, 'conditions' => array('id' => $user_id)));
        }


        $user -> logged = $logged;
        return $user;
    }

}

//We call the applyFilter() method OUTSIDE the class to create our new filter rules
Pessoas::applyFilter('save', function($self, $params, $chain) {
	//Temporarily store our entity object so that we can manipulate it
	$record = $params['entity'];

	//If an id doesn't exist yet, then we know we're saving for the first time. If a
	//password is provided, we need to hash it
	if (!$record -> id && !empty($record -> senha)) {
		$record -> senha = \lithium\util\String::hash($record -> senha);
	}
	//Write the modified object back to $params
	$params['entity'] = $record;
	//Allow the next filter to be run
	return $chain -> next($self, $params, $chain);
});
?>
