<?php
/**
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


 */

namespace app\models;

use \lithium\security\Auth;

use \lithium\util\Validator;
use \lithium\data\Connections;
use \lithium\storage\Session;

class Tarefas extends \lithium\data\Model {

    public $belongsTo = array('Pessoas', 'Canais');

    public $validates = array(
       'pessoa_id' => array(
           array('notEmpty', 'message' => 'insira um número do canal, ex.: gpio16 = canal 16'),
       ),
       'canai_id' => array(
           array('notEmpty', 'message' => 'selecione um canal a ser controlado'),
       ),
     );


     public static function getTarefas() {
        $tarefas = array();
        $conditions = array('sn_excluido'=>'False' );
        $tarefas = Tarefas::find('all',array('conditions'=>$conditions,'with'=>array('Canais')));
        $tarefas = $tarefas->to('array');
        //print_r($canais);exit;
        //$canais = array('16'=>'gpio16','20'=>'gpio20','21'=>'gpio21',);
        return compact('tarefas');
    }


}


?>
