/*
 * CIR: Controle Integrado Remoto (Remote Control Integrated)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.

 Código firmware utilizado pelo arduino é similar ao código exemplo:

 Arduino Yún Bridge example

 Para utilizar este código é necessário o hardware Arduino Ethernet Shield.

 Os comandos criados são:

 * "/arduino/digital/13"     -> digitalRead(13)
 * "/arduino/digital/13/1"   -> digitalWrite(13, HIGH)
 * "/arduino/analog/2/123"   -> analogWrite(2, 123)
 * "/arduino/analog/2"       -> analogRead(2)
 * "/arduino/mode/13/input"  -> pinMode(13, INPUT)
 * "/arduino/mode/13/output" -> pinMode(13, OUTPUT)



 */

#include <SPI.h>
#include <Ethernet.h>

// size of buffer used to capture HTTP requests
#define REQ_BUF_SZ   60

// MAC address from Ethernet shield sticker under board
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 108); // IP address, may need to change depending on network
IPAddress gateway(192,168,1,1);
EthernetServer server(8889);  // create a server at port 80

char HTTP_req[REQ_BUF_SZ] = {0}; // buffered HTTP request stored as null terminated string
char req_index = 0;              // index into HTTP_req buffer
boolean LED_state[4] = {0}; // stores the states of the LEDs

void setup() {

  Serial.begin(9600);       // for debugging

  // Ethernet startup

  pinMode(6, OUTPUT);
  digitalWrite(6, LOW);
  Ethernet.begin(mac, ip, gateway);  // initialize Ethernet device
  Serial.println("SUCCESS - Serviço de rede iniciado.");
  digitalWrite(6, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)

  server.begin(); // start to listen for clients
}

void loop() {
  // Get clients coming from server
  EthernetClient client = server.available();

  // There is a new client?
  if (client) {
    // Process request
    process(client);

    // Close connection and free resources.
    client.stop();
  }

  delay(50); // Poll every 50ms
}

void process(EthernetClient client) {
  // read the command
  String command = client.readStringUntil('/'); //pega a primeira barra

  command = client.readStringUntil('/'); // arduino/data

  Serial.println("Solicitação recebida");
  Serial.println(command);

  // is "digital" command?
  if (command == "arduino"){
      command = client.readStringUntil('/'); // digital/analog/mode
      Serial.println(command);
      if (command == "digital") {
        digitalCommand(client);
      }

      // is "analog" command?
      if (command == "analog") {
        analogCommand(client);
      }

      // is "mode" command?
      if (command == "mode") {
        modeCommand(client);
      }
  }
}

void digitalCommand(EthernetClient client) {
  int pin, value;

  // Read pin number
  pin = client.parseInt();
  Serial.println(pin);
  // If the next character is a '/' it means we have an URL
  // with a value like: "/digital/13/1"
  if (client.read() == '/') {

    value = client.parseInt();
    Serial.println(value);
    digitalWrite(pin, value);
  }
  else {
    value = digitalRead(pin);
  }

  // Send feedback to client

  client.print("{\"status\" : \"");
  client.print(value);
  client.print("\"}");

  //Serial.println(value);

}

void analogCommand(EthernetClient client) {
  int pin, value;

  // Read pin number
  pin = client.parseInt();

  // If the next character is a '/' it means we have an URL
  // with a value like: "/analog/5/120"
  if (client.read() == '/') {
    // Read value and execute command
    value = client.parseInt();
    analogWrite(pin, value);

    // Send feedback to client


    client.print("{\"status\" : \"");
    client.print(value);
    client.print("\"}");


  }
  else {
    // Read analog pin
    value = analogRead(pin);

    // Send feedback to client

    client.print("{\"status\" : \"");
    client.print(value);
    client.print("\"}");


  }
}

void modeCommand(EthernetClient client) {
  int pin;

  // Read pin number
  pin = client.parseInt();

  // If the next character is not a '/' we have a malformed URL
  if (client.read() != '/') {
    client.println(F("error"));
    return;
  }

  String mode = client.readStringUntil(' ');

  if (mode == "input") {
    pinMode(pin, INPUT);
    // Send feedback to client
    client.print("{\"status\" : \"");
    client.print(F("INPUT!"));
    client.print("\"}");

    return;
  }

  if (mode == "output") {
    pinMode(pin, OUTPUT);
    // Send feedback to client
    client.print("{\"status\" : \"");
    client.print(F("OUTPUT!"));
    client.print("\"}");

    return;
  }

  client.print(F("error: invalid mode "));
  client.print(mode);
}


