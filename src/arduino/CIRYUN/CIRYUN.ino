/**
 * CIR: Controle Integrado Remoto (Integrated Remote Control)
 *
 * @copyright     Copyright 2015, Bod Garage (http://bodgarage.repofy.com)
 * @license
 *  This file is part of CIR .

    CIR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CIR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CIR.  If not, see <http://www.gnu.org/licenses/>.


  O código utilizado pelo arduino é similar ao código exemplo:

  Arduino Yún Bridge example

  Para utilizar este código é necessário o hardware Arduino Yun Shield.

  Os comandos criados são:

 *  "/arduino/digital/13"     -> digitalRead(13)
 *  "/arduino/digital/13/1"   -> digitalWrite(13, HIGH)
 *  "/arduino/analog/2/123"   -> analogWrite(2, 123)
 *  "/arduino/analog/2"       -> analogRead(2)
 *  "/arduino/mode/13/input"  -> pinMode(13, INPUT)
 *  "/arduino/mode/13/output" -> pinMode(13, OUTPUT)


 */

#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>


// Listen to the default port 5555, the Yún webserver
// will forward there all the HTTP requests you send
YunServer server;

void setup() {
  // Bridge startup
  pinMode(13, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  //server.listenOnLocalhost();
  //server.noListenOnLocalhost();
  server.begin();
}

void loop() {
  // Get clients coming from server
  YunClient client = server.accept();

  // There is a new client?
  if (client) {
    // Process request
    process(client);

    // Close connection and free resources.
    client.stop();
  }

  delay(50); // Poll every 50ms
}

void process(YunClient client) {
  // read the command
  String command = client.readStringUntil('/');

  // is "digital" command?
  if (command == "digital") {
    digitalCommand(client);
  }

  // is "analog" command?
  if (command == "analog") {
    analogCommand(client);
  }

  // is "mode" command?
  if (command == "mode") {
    modeCommand(client);
  }
}

void digitalCommand(YunClient client) {
  int pin, value;

  // Read pin number
  pin = client.parseInt();

  // If the next character is a '/' it means we have an URL
  // with a value like: "/digital/13/1"
  if (client.read() == '/') {
    value = client.parseInt();
    digitalWrite(pin, value);
  }
  else {
    value = digitalRead(pin);
  }

  // Send feedback to client

  client.print("{\"status\" : \"");
  client.print(value);
  client.print("\"}");

  // Update datastore key with the current pin value
  String key = "D";
  key += pin;
  Bridge.put(key, String(value));
}

void analogCommand(YunClient client) {
  int pin, value;

  // Read pin number
  pin = client.parseInt();

  // If the next character is a '/' it means we have an URL
  // with a value like: "/analog/5/120"
  if (client.read() == '/') {
    // Read value and execute command
    value = client.parseInt();
    analogWrite(pin, value);

    // Send feedback to client
    client.print("{\"status\" : \"");
    client.print(value);
    client.print("\"}");

    // Update datastore key with the current pin value
    String key;
    key += pin;
    Bridge.put(key, String(value));
  }
  else {
    // Read analog pin
    value = analogRead(pin);

    // Send feedback to client
    //client.print(F("Pin A"));
    //client.print(pin);
    //client.print(F(" reads analog "));
    client.print("{\"status\" : \"");
    client.print(value);
    client.print("\"}");

    // Update datastore key with the current pin value
    String key = "A";
    key += pin;
    Bridge.put(key, String(value));
  }
}

void modeCommand(YunClient client) {
  int pin;

  // Read pin number
  pin = client.parseInt();

  // If the next character is not a '/' we have a malformed URL
  if (client.read() != '/') {
    client.println(F("error"));
    return;
  }

  String mode = client.readStringUntil('\r');

   if (mode == "input") {
    pinMode(pin, INPUT);
    // Send feedback to client
    client.print("{\"status\" : \"");
    client.print(F("INPUT!"));
    client.print("\"}");

    return;
  }

  if (mode == "output") {
    pinMode(pin, OUTPUT);
    // Send feedback to client
    client.print("{\"status\" : \"");
    client.print(F("OUTPUT!"));
    client.print("\"}");

    return;
  }

  client.print(F("error: invalid mode "));
  client.print(mode);
}


